#use wml::debian::template title="Licença Pública Geral GNU" NOCOPYRIGHT="true"
#use wml::debian::translation-check translation="d109ac4197d816202547b41d15ccf7b9e9e35b8e"

<p><strong>
Versão 2, junho de 1991
</strong></p>

<pre>
Copyright (C) 1989, 1991 Free Software Foundation, Inc.  
51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA

A qualquer pessoa é permitido copiar e distribuir cópias
desse documento de licença, desde que sem qualquer alteração.

</pre>

<h3><a name="preamble"></a><a name="SEC2">Preâmbulo</a></h3>

<p>
  As licenças para a maioria dos softwares foram projetadas para tirar
a liberdade de compartilhá-lo e alterá-lo. Por outro lado, a Licença
Pública Geral GNU destina-se a garantir a sua liberdade de compartilhar
e alterar software livre – para garantir que o software seja livre para
todos(as) os(as) usuários(as). Esta Licença Pública Geral aplica-se à maioria
dos softwares da Free Software Foundation e a qualquer outro programa cujos(as)
autores(as) se comprometerem a usá-la. (Alguns outros softwares da Free
Software Foundation são cobertos pela Licença Pública Geral Menor GNU
em vez desta). Você também pode aplicá-la aos seus programas.
</p>

<p>
  Quando falamos de software livre, estamos nos referindo à liberdade,
não ao preço. Nossas Licenças Públicas Gerais são projetadas para
garantir que você tenha a liberdade de distribuir cópias de software
livre (e cobrar por este serviço, se desejar), que você receba o
código-fonte ou possa obtê-lo, se desejar, que você possa mudar o
software ou usar partes dele em novos programas livres; e que você
saiba que pode fazer essas coisas.
</p>

<p>
  Para proteger seus direitos, precisamos fazer restrições que proíbem
qualquer um(a) de negar esses direitos ou pedir-lhe que os renuncie. Essas
restrições se traduzem em certas responsabilidades para você, se você
distribuir cópias do software, ou se você modificá-lo.
</p>

<p>
  Por exemplo, se você distribuir cópias de tal programa, gratuitamente
ou por uma taxa, você deve dar aos(às) destinatários(as) todos os direitos que
você possui. Você deve se certificar de que eles(as), também, recebam ou
possam obter o código-fonte. E você deve mostrar esses termos para que
eles(as) conheçam seus direitos.
</p>

<p>
  Protegemos seus direitos com duas etapas: (1) com direitos autorais
do software e (2) oferecendo-lhe esta Licença, que lhe dá a permissão
legal para copiá-lo, distribuí-lo e/ou modificá-lo.
</p>

<p>
  Além disso, para a nossa proteção e a de cada autor(a), queremos garantir
que todos(as) entendam que não há garantia para este software livre. Se o
software for modificado por outra pessoa e passado para frente, queremos
que seus(suas) destinatários(as) saibam que o que têm não é o original, de
modo que qualquer problema introduzido por outros(as) não refletirá sobre a
reputação dos(as) autores(as) originais.
</p>

<p>
  Finalmente, qualquer programa livre é ameaçado constantemente por
patentes de software. Desejamos evitar o perigo de que redistribuidores(as)
de um programa livre obtenham, individualmente, licenças de patentes,
efetivamente tornando o programa proprietário. Para evitar isso, deixamos
claro que qualquer patente deve ser licenciada para uso livre de todos(as) ou 
não licenciada.
</p>

<p>
  Os termos e condições precisos para a cópia, distribuição e
modificação seguem abaixo.
</p>


<h3><a name="terms"></a><a name="SEC3">TERMOS E CONDIÇÕES PARA CÓPIA, DISTRIBUIÇÃO E MODIFICAÇÃO</a></h3>


<a name="section0"></a><p>
<strong>0.</strong>

 Esta Licença aplica-se a qualquer programa ou outro trabalho que
contenha um aviso colocado pelo(a) detentor(a) dos direitos autorais dizendo
que pode ser distribuído nos termos desta Licença Pública Geral. O
“programa”, abaixo, refere-se a qualquer programa ou trabalho, e um
“trabalho baseado no programa” significa o programa ou qualquer trabalho
derivado sob a lei de direitos autorais: ou seja, um trabalho que contenha
o programa ou uma parte de isso, quer literalmente ou com modificações
e/ou traduzido para outro idioma. (Daqui em diante, a tradução está
incluída sem limitação no termo “modificação”). Cada licenciado(a) é
endereçado como “você”.
</p>

<p>
As atividades que não sejam de cópia, distribuição e modificação não
são cobertas por esta Licença; elas estão fora do seu alcance. O ato
de executar o programa não é restrito e a saída do programa está coberta
somente se o seu conteúdo constituir um trabalho baseado no programa
(independente de ter sido feito executando o programa). Se isso é
verdade ou não, depende do que o programa faz.
</p>

<a name="section1"></a><p>
<strong>1.</strong>
 Você pode copiar e distribuir cópias literais do código-fonte do
programa na forma como você o recebe, em qualquer meio, desde que você
publique de forma consistente e apropriada em cada cópia um aviso de
direitos autorais e um aviso legal de garantia; mantenha intactos todos os
avisos que se referem a esta Licença e à ausência de qualquer garantia; e
conceda a qualquer outro(a) destinatário(a) do programa uma cópia desta Licença,
juntamente com o programa.
</p>

<p>
Você pode cobrar uma taxa pelo ato físico de transferir uma cópia e, a
seu critério, pode oferecer proteção de garantia em troca de uma taxa.

</p>

<a name="section2"></a><p>
<strong>2.</strong>
 Você pode modificar sua cópia ou cópias do programa ou qualquer parte
dela, formando assim um trabalho com base no programa e copiar e
distribuir tais modificações ou trabalho nos termos da Seção 1 acima,
desde que você também atenda a todas essas condições:
</p>

<dl>
  <dt>&nbsp;</dt>
    <dd>
      <strong>a)</strong>

      Você deve fazer com que os arquivos modificados levem avisos
      proeminentes afirmando que você mudou os arquivos e a data de
      qualquer alteração.
    </dd>
  <dt>&nbsp;</dt>
    <dd>
      <strong>b)</strong>
      Você deve fazer com que qualquer trabalho que você distribuir
      ou publicar, que, no todo ou em parte, contenha o programa ou
      seja derivado dele, seja licenciado sem custo para todos os
      terceiros nos termos desta Licença.
    </dd>
  <dt>&nbsp;</dt>
    <dd>

      <strong>c)</strong>
      Se o programa modificado normalmente lê comandos de forma
      interativa quando executado, você deve fazer com que ele,
      quando iniciada execução para esse uso interativo da maneira
      mais simples e mais usual, imprima ou exiba um anúncio incluindo
      um aviso de direitos autorais apropriado e um aviso de que não
      há garantia (ou então, dizendo que você fornece uma garantia) e
      que os(as) usuários(as) podem redistribuir o programa sob essas condições
      e informar ao(à) usuário(a) como visualizar uma cópia desta Licença.
      (Exceção: se o próprio programa é interativo, mas normalmente
      não imprime esse anúncio, seu trabalho com base no programa não
      é obrigado a imprimir um anúncio).
    </dd>
</dl>

<p>
Esses requisitos se aplicam ao trabalho modificado como um todo. Se as
seções identificáveis desse trabalho não forem derivadas do programa e
puderem ser razoavelmente consideradas independentes e separadas em si
mesmas, então esta Licença e seus termos não se aplicam a essas seções
quando você as distribui como trabalhos separados. Mas quando você
distribui as mesmas seções como parte de um todo que é um trabalho
baseado no programa, a distribuição do todo deve estar nos termos desta
Licença, cujas permissões para outros(as) licenciados(as) se estendem ao todo,
e assim a cada um e todas as partes independentemente de quem a escreveu.
</p>

<p>
Assim, não é intenção desta seção reivindicar direitos ou contestar seus
direitos de trabalho escritos inteiramente por você; Em vez disso, a
intenção é exercer o direito de controlar a distribuição de trabalhos
derivados ou coletivos com base no programa.
</p>

<p>

Além disso, a mera agregação de outro trabalho não baseado no programa
com o programa (ou com um trabalho baseado no programa) em um volume de
um meio de armazenamento ou distribuição não traz o outro trabalho sob
o escopo desta Licença.
</p>

<a name="section3"></a><p>
<strong>3.</strong>
 Você pode copiar e distribuir o programa (ou um trabalho baseado nele
sob a Seção 2) em código objeto ou forma de executável nos termos da
Seção 1 ou 2 acima, desde que você também faça um dos seguinte:
</p>

<!-- we use this doubled UL to get the sub-sections indented, -->
<!-- while making the bullets as unobvious as possible. -->

<dl>
  <dt>&nbsp;</dt>
    <dd>

      <strong>a)</strong>
      Acompanhe-o com o código-fonte completo correspondente, legível
      por máquina, que deve ser distribuído nos termos das Seções 1 e 2
      acima em um meio usado habitualmente para o intercâmbio de
      software; ou,
    </dd>
  <dt>&nbsp;</dt>
    <dd>
      <strong>b)</strong>
      Acompanhe-o com uma oferta escrita, válida por pelo menos três
      anos, para dar a terceiros(as), por uma cobrança não superior ao seu
      custo de realizar a distribuição de fontes fisicamente, uma cópia
      completa legível por máquina do código-fonte correspondente, a
      ser distribuída sob os termos das Seções 1 e 2 acima em um meio
      comum usado para intercâmbio de software; ou,
    </dd>
  <dt>&nbsp;</dt>

    <dd>
      <strong>c)</strong>
      Acompanhe-o com as informações que recebeu quanto à oferta para
      distribuir o código-fonte correspondente. (Esta alternativa é
      permitida somente para distribuição não comercial e somente se
      você recebeu o programa em código objeto ou forma de executável
      com tal oferta, de acordo com a Subsecção b acima).
    </dd>
</dl>

<p>
O código-fonte para um trabalho significa a forma preferida do trabalho
para fazer modificações nele. Para um trabalho executável, o código-fonte
completo significa todo o código-fonte para todos os módulos que ele
contenha, além de qualquer arquivo de definição de interface associado,
além dos scripts usados para controlar a compilação e a instalação do
executável. No entanto, como uma exceção especial, o código-fonte
distribuído não precisa incluir qualquer coisa que seria normalmente
distribuída (na forma fonte ou binária) com os principais componentes
(compilador, kernel e assim por diante) do sistema operacional no qual
o executável é executado, a menos que esse componente acompanhe o
executável.
</p>

<p>
Se a distribuição do código executável ou código objeto é feita
oferecendo acesso a cópia de um local designado, oferecer acesso
equivalente para copiar o código-fonte do mesmo local conta como
distribuição do código-fonte, ainda que terceiros(as) não sejam obrigados(as)
a copiar o fonte juntamente com o código objeto.
</p>

<a name="section4"></a><p>
<strong>4.</strong>
 Você não pode copiar, modificar, sublicenciar, distribuir ou transferir
o programa, exceto conforme expressamente previsto nesta Licença Pública
Geral. Qualquer tentativa de copiar, modificar, sublicenciar, distribuir
ou transferir o programa é anulada e terminará automaticamente seus
direitos de usar o programa sob esta Licença. No entanto, as partes que
receberam cópias ou direitos de uso de cópias de você sob esta Licença
Pública Geral não terão suas licenças terminadas, desde que essas partes
permaneçam em plena conformidade.
</p>

<a name="section5"></a><p>
<strong>5.</strong>
 Você não é obrigado(a) a aceitar esta Licença, já que não a assinou.
No entanto, nada mais lhe concede permissão para modificar ou distribuir
o programa ou seus trabalhos derivados. Essas ações são proibidas por lei
se você não aceitar esta Licença. Portanto, ao modificar ou distribuir o
programa (ou qualquer trabalho baseado no programa), você indica sua
aceitação desta Licença para fazê-lo e todos os seus termos e condições
para copiar, distribuir ou modificar o programa ou trabalhar com base
nela.
</p>

<a name="section6"></a><p>
<strong>6.</strong>
 Cada vez que você redistribuir o programa (ou qualquer trabalho baseado
no programa), o(a) destinatário(a) recebe automaticamente uma licença do(a)
licenciador(a) original para copiar, distribuir ou modificar o programa
sujeito a estes termos e condições. Você não pode impor restrições
adicionais ao exercício dos direitos dos(as) destinatários(as) aqui concedidos.
Você não é responsável por fazer cumprir a conformidade por terceiros(as)
com esta Licença.
</p>

<a name="section7"></a><p>
<strong>7.</strong>
 Se, como consequência de uma sentença judicial ou alegação de violação
de patente ou por qualquer outro motivo (não limitado a questões de
patentes), são impostas condições (seja por ordem judicial, acordo ou
de outra forma) que contradizem as condições desta Licença, elas não lhe
eximem das condições desta Licença. Se você não pode distribuir para
satisfazer simultaneamente suas obrigações sob esta Licença e quaisquer
outras obrigações pertinentes, então, como consequência, você não pode
distribuir o programa. Por exemplo, se uma licença de patente não
permitisse a redistribuição sem royalties do programa por todos(as) aqueles(as)
que recebem cópias direta ou indiretamente por você, então a única
maneira pela qual você poderia satisfazê-la e a essa Licença seria
abster-se totalmente da distribuição do programa.
</p>

<p>
Se qualquer parte desta seção for considerada inválida ou não aplicável
em qualquer circunstância particular, o restante da seção se aplica e a
seção como um todo se aplica em outras circunstâncias.
</p>

<p>
Não é o propósito desta seção induzir você a infringir quaisquer patentes
ou outras reivindicações de direito de propriedade ou a contestar a
validade de tais reivindicações; esta seção tem o único propósito de
proteger a integridade do sistema de distribuição de software livre,
que é implementado por práticas de licenças públicas. Muitas pessoas
fizeram contribuições generosas para a ampla gama de software distribuído
por esse sistema com base na aplicação consistente desse sistema; cabe ao
autor(a)/doador(a) decidir se ele ou ela tem interesse em distribuir software
através de qualquer outro sistema e um(a) licenciado(a) não pode impor essa
escolha.

</p>

<p>
Esta seção visa deixar bem claro o que se acredita ser uma consequência
do restante desta Licença.
</p>

<a name="section8"></a><p>
<strong>8.</strong>
 Se a distribuição e/ou uso do programa for restrito em certos países,
seja por patentes ou por interfaces protegidas por direitos autorais,
o(a) detentor(a) de direitos autorais original que coloca o programa sob esta
Licença pode adicionar uma limitação de distribuição geográfica explícita,
excluindo esses países, para que a distribuição seja permitida somente em
ou entre países que não estão assim excluídos. Nesse caso, esta Licença
incorpora a limitação como se estivesse escrita no corpo desta Licença.
</p>

<a name="section9"></a><p>
<strong>9.</strong>
 A Free Software Foundation pode publicar versões periódicas e/ou novas
da Licença Pública Geral de tempos em tempos. Essas novas versões serão
semelhantes em espírito à versão atual, mas podem diferir em detalhes
para resolver novos problemas ou preocupações.

</p>

<p>
Cada versão recebe um número de versão distinto. Se o programa
especificar um número de versão desta Licença que se aplica a ele,
“ou qualquer versão posterior” dela, você tem a opção de seguir os
termos e condições dessa versão ou de qualquer versão posterior
publicada pela Free Software Foundation. Se o programa não especificar
um número de versão desta Licença, você pode escolher qualquer versão
publicada pela Free Software Foundation.
</p>

<a name="section10"></a><p>
<strong>10.</strong>
 Se você deseja incorporar partes do programa em outros programas livres
cujas condições de distribuição são diferentes, escreva para o(a) autor(a) para
pedir permissão. Para o software que é protegido por direitos autorais
pela Free Software Foundation, escreva para a Free Software Foundation;
às vezes fazemos exceções para isso. Nossa decisão será orientada pelos
dois objetivos de preservar o status livre de todos os derivados do nosso
software livre e de promover o compartilhamento e reuso de software em
geral.
</p>

<a name="section11"></a><p><strong>SEM GARANTIA</strong></p>

<p>

<strong>11.</strong>
 PORQUE O PROGRAMA É LICENCIADO GRATUITAMENTE, NÃO HÁ NENHUMA GARANTIA
PARA O PROGRAMA, NA EXTENSÃO PERMITIDA PELA LEI APLICÁVEL. EXCETO QUANDO
TUDO INDICADO POR ESCRITO, OS(AS) DETENTORES(AS) DOS DIREITOS AUTORAIS E/OU
OUTRAS PARTES FORNECEM O PROGRAMA SEM GARANTIA DE QUALQUER TIPO, EXPRESSA
OU IMPLÍCITA, INCLUINDO, MAS NÃO SE LIMITANDO A, GARANTIAS IMPLÍCITAS DE
COMERCIALIZAÇÃO E ADEQUAÇÃO PARA UM FIM ESPECÍFICO. TODO O RISCO SOBRE A
QUALIDADE E O DESEMPENHO DO PROGRAMA ESTÁ COM VOCÊ. SE O PROGRAMA
APRESENTAR DEFEITO, VOCÊ ASSUME O CUSTO DE TODA A MANUTENÇÃO, REPARAÇÃO
OU CORREÇÃO NECESSÁRIA.
</p>

<a name="section12"></a><p>
<strong>12.</strong>
 EM NENHUM CASO, A MENOS QUE EXIGIDO PELA LEI APLICÁVEL OU ACORDADO POR
ESCRITO, QUALQUER DETENTOR(A) DE DIREITOS AUTORAIS, OU QUALQUER OUTRA PARTE
QUE PODE MODIFICAR E/OU REDISTRIBUIR O PROGRAMA COMO PERMITIDO ACIMA, SE
RESPONSABILIZARÁ POR DANOS, INCLUINDO QUALQUER DANO GERAL, ESPECIAL,
INCIDENTAL OU CONSEQUENCIAL QUE SURGIR DO USO OU INCAPACIDADE DE USAR O
PROGRAMA (INCLUINDO, MAS NÃO SE LIMITANDO À PERDA DE DADOS OU DADOS QUE
SÃO PRESTADOS IMPRECISAMENTE OU PERDAS SUSTENTADAS POR VOCÊ OU TERCEIROS(AS)
OU UMA FALHA DO PROGRAMA A OPERAR COM OUTROS PROGRAMAS), MESMO SE TAL
DETENTOR(A) OU OUTRA PARTE TENHA SIDO AVISADO(A) DA POSSIBILIDADE DE TAIS
DANOS.
</p>

<h3>FIM DOS TERMOS E CONDIÇÕES</h3>

<h3><a name="howto"></a><a name="SEC4">Como aplicar esses termos a seus novos programas</a></h3>

<p>
  Se você desenvolver um novo programa, e deseja que seja do melhor uso
possível para o público, a melhor maneira de conseguir isso é torná-lo
software livre, o qual todos(as) podem redistribuir e mudar de acordo sob
esses termos.
</p>

<p>
  Para fazer isso, anexe os seguintes avisos ao programa. É mais seguro
anexá-los ao início de cada arquivo-fonte para transmitir com maior
eficiência a exclusão da garantia; e cada arquivo deve ter pelo menos a
linha “copyright” e apontar para onde o aviso completo é encontrado.
</p>

<pre>
<var>one line to give the program's name and an idea of what it does.</var>
Copyright (C) <var>yyyy</var>  <var>name of author</var>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
</pre>

# While the above disclaimer is the only one legally valid, let's
# translated for better understanding of the reader -- Rafael Fontenelle
Em português, seria o equivalente a:

<pre>
<var>uma linha para dar o nome do programa e uma ideia do que faz.</var>
Copyright (C) <var>aaaa</var>  <var>nome do(a) autor(a)</var>

Este programa é um software livre; você pode redistribuí-lo e/ou
modificá-lo sob os termos da Licença Pública Geral GNU, conforme
publicado pela Free Software Foundation, seja a versão 2 da Licença
ou (a seu critério) qualquer versão posterior.

Este programa é distribuído na esperança de que seja útil,
mas SEM QUALQUER GARANTIA; nem mesmo a garantia implícita de
COMERCIALIZAÇÃO OU ADEQUAÇÃO A UM DETERMINADO PROPÓSITO. Veja a
Licença Pública Geral GNU para obter mais detalhes.

Você deve ter recebido uma cópia da Licença Pública Geral GNU
junto com este programa. Se não, escreva para a Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
</pre>

<p>
Adicione também informações sobre como entrar em contato com você por
correio eletrônico e papel.
</p>

<p>
Se o programa é interativo, faça com que ele emita um breve aviso como
este quando ele começar em um modo interativo:
</p>

<pre>
Gnomovision version 69, Copyright (C) <var>year</var> <var>name of author</var>

Gnomovision comes with ABSOLUTELY NO WARRANTY; for details
type 'show w'.  This is free software, and you are welcome
to redistribute it under certain conditions; type 'show c' 
for details.
</pre>

# While the above disclaimer is the only one legally valid, let's
# translated for better understanding of the reader -- Rafael Fontenelle
Em português, seria o equivalente a:

<pre>
Gnomovision versão 69, Copyright (C) <var>ano</var> <var>nome do(a) autor(a)</var>

Gnomovision vem com ABSOLUTAMENTE NENHUMA GARANTIA;
para detalhes, digite 'show w'. Este é um software livre,
e você pode redistribuí-lo sob certas condições;
digite 'show c' para obter detalhes.
</pre>

<p>
Os comandos hipotéticos <samp>'show w'</samp> e <samp>'show c'</samp>
devem mostrar as partes apropriadas da Licença Pública Geral. Claro,
os comandos do seu programa podem ser diferentes de <samp>'show w'</samp>
e <samp>'show c'</samp>; eles podem até mesmo ser cliques de mouse ou
itens de menu – o que quer que seja adequado para seu programa.
</p>

<p>
Você também deve fazer com que o(a) seu(sua) empregador(a) (se você trabalha como
programador(a)) ou a escola, se for o caso, assine uma “aviso legal de
direitos autorais” para o programa, se necessário. Aqui está uma
amostra; altere os nomes:
</p>


<pre>
Yoyodyne, Inc., hereby disclaims all copyright
interest in the program 'Gnomovision'
(which makes passes at compilers) written 
by James Hacker.

<var>signature of Ty Coon</var>, 1 April 1989
Ty Coon, President of Vice
</pre>

Em português, seria o equivalente a:

<pre>
Yoyodyne, Inc., aqui renuncia todos os
interesses de direitos autorais sobre o
programa 'Gnomovision' (que executa
interpretações em compiladores) escrito
por James Hacker.

<var>assinatura de Ty Coon</var>, 1º de abril de 1989
Ty Coon, Presidente da Vice
</pre>
