#use wml::debian::template title="Debian GNU/Linux 2.2 -- Errata" BARETITLE=true
#use wml::debian::translation-check translation="534d1b782cfb92f46dc41fd064f779fffc329b12"

<p><strong>O Debian GNU/Linux 2.2 ficou obsoleto após o
<a href="../woody/">Debian GNU/Linux 3.0 ("woody")</a>.
As atualizações de segurança foram descontinuadas a partir de 30 de junho de
2003.</strong>
Por favor, veja os
<a href="https://lists.debian.org/debian-devel-announce/2003/debian-devel-announce-200302/msg00010.html">\
resultados da pesquisa do time de segurança</a> para mais informações.</p>

<h2>Problemas de segurança</h2>

<p>O time de segurança do Debian lança atualizações para os pacotes da versão
estável (stable) nos quais identificaram problemas relacionados à segurança. Por favor,
consulte as <a href="$(HOME)/security/">páginas de segurança</a>  para
informações sobre qualquer questão de segurança identificada no "potato".</p>

<p>Se você usa o APT, adicione a seguinte linha em
<tt>/etc/apt/sources.list</tt> para que possa acessar as últimas atualizações
de segurança:</p>

<pre>
  deb http://security.debian.org/ potato/updates main contrib non-free
</pre>

<p>Depois disso, execute <kbd>apt-get update</kbd> seguido por
<kbd>apt-get upgrade</kbd>.</p>

<h2>Versões pontuais</h2>

<p>Às vezes, no caso de vários problemas críticos ou atualizações de segurança,
a versão já lançada é atualizada. Geralmente, essas atualizações são
indicadas como versões pontuais.</p>

<ul>
  <li>A primeira versão pontual, 2.2r1, foi lançada em <a href="$(HOME)/News/2000/20001114">14 de novembro de 2000</a>.</li>
  <li>A segunda versão pontual, 2.2r2, foi lançada em <a href="$(HOME)/News/2000/20001205">3 de dezembro de 2000</a>.</li>
  <li>A terceira versão pontual, 2.2r3, foi lançada em <a href="$(HOME)/News/2001/20010417">17 de abril de 2001</a>.</li>
  <li>A quarta versão pontual, 2.2r4, foi lançada em <a href="$(HOME)/News/2001/20011105">5 de novembro de 2001</a>.</li>
  <li>A quinta versão pontual, 2.2r5, foi lançada em <a href="$(HOME)/News/2002/20020110">10 de janeiro de 2002</a>.</li>
  <li>A sexta versão pontual, 2.2r6, foi lançada em <a href="$(HOME)/News/2002/20020403">3 de abril de 2002</a>.</li>
  <li>A sétima versão pontual, 2.2r7, foi lançada em <a href="$(HOME)/News/2002/20020713">13 de julho de 2002</a>.</li>
</ul>

<p>
Veja o <a href="http://archive.debian.org/debian/dists/potato/ChangeLog">
ChangeLog</a> (e o
<a href="http://archive.debian.org/debian-non-US/dists/potato/non-US/ChangeLog">
ChangeLog para não US</a>) para mais detalhes sobre as
alterações.</p>

<p>Correções para a versão estável (stable) lançada geralmente passam por um
período extensivo de testes antes de serem aceitas no repositório.
Entretanto, essas correções estão disponíveis no repositório
<a href="http://archive.debian.org/debian/dists/potato-proposed-updates/">
dists/potato-proposed-updates</a> de qualquer espelho de repositório do Debian
(e no mesmo local em nosso
<a href="http://archive.debian.org/debian-non-US/dists/potato-proposed-updates/">
servidor não US</a> e seus espelhos).</p>

<p>Se você usa o <tt>apt</tt> para atualizar seus pacotes, você pode instalar
as atualizações propostas pela adição da seguinte linha ao arquivo
<tt>/etc/apt/sources.list</tt>:</p>

# Provavelmente esses links devem ser localizados nas versões traduzidas,
# se possível.
<pre>
  \# adições propostas para uma versão pontual 2.2
  deb http://archive.debian.org dists/potato-proposed-updates/
  deb http://archive.debian.org/debian-non-US dists/potato-proposed-updates/
</pre>

<p>Depois disso, execute <kbd>apt-get update</kbd> seguido por
<kbd>apt-get upgrade</kbd>.</p>

# alguém por favor verifique se isto ainda é correto para 2.2r3:
#
# <h2>Construindo imagens de CD</h2>
#
# <p>Pessoas que desejem criar imagens personalizadas de CD para 2.2r2
# precisarão usar a versão CVS de "debian-cd" e <strong>não</strong> a versão
# atualmente instalada no potato.</p>

<h2>Suporte não oficial à instalação internacionalizada</h2>

<p>Uma versão internacionalizada do sistema de instalação para i386 está
disponível (https://people.debian.org/~aph/current-i18n/) para
teste e utilização. Você precisa usar os tipos "idepci" ou "compact".</p>

<p>Devido a problemas remanescentes com o sistema de instalação
internacionalizado, é pouco provável que seja incluído oficialmente em uma
versão pontual do Potato. Só podemos esperar que ele seja preparado para o
 <a href="../woody/">woody</a>.</p>

<h2>Suporte não oficial a ReiserFS</h2>

<p>Aparentemente há pelo menos duas versões não oficiais do
sistema de instalação para i386 com suporte a ReiserFS.</p>

<p>Uma versão é do John H. Robinson, IV, e está disponível com
algumas instruções em <a
href="http://chao.ucsd.edu/debian/boot-floppies/">
http://chao.ucsd.edu/debian/boot-floppies/</a>.</p>

<p>A outra é do Marc A. Volovic e está disponível em
http://www.bard.org.il/~marc/linux/rfs/.</p>


<h2>Suporte não oficial ao Linux Kernel 2.4</h2>

<p>
Adrian Bunk forneceu um conjunto de pacotes para sistemas Potato i386
que permitem a compatibilidade para o Linux Kernel 2.4.x. Por favor, leia <a
href="http://www.fs.tum.de/~bunk/kernel-24.html"> para instruções,
avisos e linhas a serem adicionadas em <code>/etc/apt/sources.list</code>.</p>
