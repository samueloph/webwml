#use wml::debian::translation-check translation="3508f9a531af1aca0b0e166eb9e274f958f07db9" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Flere sårbarheder blev opdaget i BIND, en DNS-serverimplementering.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2795">CVE-2022-2795</a>

    <p>Yehuda Afek, Anat Bremler-Barr og Shani Stajnrod opdagede at en fejl i 
    resolverkoden kunne medføre at named brugte al for megen tid på at behandle 
    store delegeringer, hvilket i betydelig grad forværede resolverens ydeevne 
    og medførte lammelsesangreb.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3080">CVE-2022-3080</a>

    <p>Maksym Odinintsev opdagede at resolveren kunne gå ned når stale cache og 
    stale answers er aktiveret med stale-answer-timeout sat til nul.  En 
    fjernangriber kunne drage nytte af fejlen til at forårsage et 
    lammelsesangreb (dæmonnedbrud) gennem særligt fremstillede forespørgsler til 
    resolveren.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-38177">CVE-2022-38177</a>

    <p>Man opdagede at DNSSEC-verifikationskode til ECDSA-algoritmen var sårbar 
    over for en hukommelseslækagefejl.  En fjernangriber kunne drage nytte af 
    fejlen til at forårsage, at BIND forbrugte ressourcer, medførende et 
    lammelsesangreb.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-38178">CVE-2022-38178</a>

    <p>Man opdagede at DNSSEC-verifikationskode til EdDSA-algoritmen var sårbar 
    over for en hukommelseslækagefejl.  En fjernangriber kunne drage nytte af 
    fejlen til at forårsage, at BIND forbrugte ressourcer, medførende et 
    lammelsesangreb.</p></li>

</ul>

<p>I den stabile distribution (bullseye), er disse problemer rettet i
version 1:9.16.33-1~deb11u1.</p>

<p>Vi anbefaler at du opgraderer dine bind9-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende bind9, se
dens sikkerhedssporingssidede på:
<a href="https://security-tracker.debian.org/tracker/bind9">\
https://security-tracker.debian.org/tracker/bind9</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5235.data"
