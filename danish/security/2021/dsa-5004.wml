#use wml::debian::translation-check translation="9021424ee668597a5b8130c5d1bb387ddc66dac3" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Adskillige sikkerhedssårbarheder er opdaget i XStream, et Java-bibliotek til 
serialisering af objekter til XML, og tilbage igen.</p>

<p>Sårbarhederne kunne gøre det muligt for en fjernangriber at indlæse og 
udføre vilkårlig kode fra en fjern vært, kun ved at manipulere med den 
behandlede inddatastream.</p>

<p>XStream selv opsætter nu som standard en whitelist, dvs. den blokerer alle 
klasser bortset fra de typer, den har eksplicitte converters for.  Den havde 
tidligere som standard en blacklist, dvs. den prøvede at blokere alle pt. 
kendte kritiske klasse hørende til Javas runtime.  Den primære årsag til 
blacklisten var kompatibilitet, da det dermed var muligt uden videre at benytte 
nyere versioner af XStream som erstatning.  Men den tilgang har fejlet.  En 
voksende liste over sikkerhedsrapporter har vist, at en blackliste generelt er 
usikker, bortset fra det faktum at former af tredjepartsbiblioteker end ikke var 
taget i betragtning.  Et blacklistscenraie bør generelt undgås, da det giver en 
falsk følelse af sikkerhed.</p>

<p>I den gamle stabile distribution (buster), er disse problemer rettet
i version 1.4.11.1-1+deb10u3.</p>

<p>I den stabile distribution (bullseye), er disse problemer rettet i
version 1.4.15-3+deb11u1.</p>

<p>Vi anbefaler at du opgraderer dine libxstream-java-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende libxstream-java, se
dens sikkerhedssporingsside på:
<a href="https://security-tracker.debian.org/tracker/libxstream-java">\
https://security-tracker.debian.org/tracker/libxstream-java</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-5004.data"
