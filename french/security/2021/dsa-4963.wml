#use wml::debian::translation-check translation="6485968c0f180fd4720e21f7f0bdc48b3c3bf4f0" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans OpenSSL, une boîte à
outils SSL (Secure Socket Layer).</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3711">CVE-2021-3711</a>

<p>John Ouyang a signalé une vulnérabilité de dépassement de tampon dans le
déchiffrement de SM2. Un attaquant en capacité de présenter du contenu SM2
pour déchiffrement à une application peut tirer avantage de ce défaut pour
modifier le comportement de l'application ou provoquer son plantage (déni de
service).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3712">CVE-2021-3712</a>

<p>Ingo Schwarze a signalé un défaut de débordement de tampon, lors du
traitement de chaînes ASN.1 dans la fonction X509_aux_print(), qui peut
avoir pour conséquence un déni de service.</p></li>

</ul>

<p>Pour plus de détails, consultez les avertissements amont :
<a href="https://www.openssl.org/news/secadv/20210824.txt">\
https://www.openssl.org/news/secadv/20210824.txt</a></p>

<p>Pour la distribution oldstable (Buster), ces problèmes ont été corrigés
dans la version 1.1.1d-0+deb10u7.</p>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 1.1.1k-1+deb11u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets openssl.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de openssl, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/openssl">\
https://security-tracker.debian.org/tracker/openssl</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4963.data"
# $Id: $
