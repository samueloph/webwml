#use wml::debian::translation-check translation="abe74ce578f17e4d517790cbcca8dfd0326dba5c" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été corrigées dans libapache2-mod-auth-openidc,
une implémentation de <q>OpenID Connect Relying Party</q> pour Apache.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20479">CVE-2019-20479</a>

<p>Validation insuffisante d’URL commençant par une oblique ou une
contre-oblique.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-32785">CVE-2021-32785</a>

<p>Plantage lors de l’utilisation d’un cache Redis non chiffré.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-32786">CVE-2021-32786</a>

<p>Vulnérabilité de redirection ouverte dans la fonction logout.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-32791">CVE-2021-32791</a>

<p>Chiffrement AES GCM utilisé dans <q>static IV</q> et AAD.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-32792">CVE-2021-32792</a>

<p>Vulnérabilité de script intersite (XSS) lors de l’utilisation de
OIDCPreservePost.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-28625">CVE-2023-28625</a>

<p>Déréférencement de pointeur NULL avec OIDCStripCookies.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 2.3.10.2-1+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libapache2-mod-auth-openidc.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libapache2-mod-auth-openidc,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libapache2-mod-auth-openidc">\
https://security-tracker.debian.org/tracker/libapache2-mod-auth-openidc</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3409.data"
# $Id: $
