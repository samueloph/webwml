#use wml::debian::translation-check translation="e8612430a9c2407c3dc0ba50f88eafa4c9a72a8b" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>La mise à jour de sécurité annoncée par la DLA 2955-1 provoquait une
régression dans named due à une correction incomplète du
<a href="https://security-tracker.debian.org/tracker/CVE-2021-25220">CVE-2021-25220</a>
quand l'option Forwarders était configurée. Des paquets de bind9 mis à jour
sont maintenant disponibles pour corriger ce problème.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
1:9.10.3.dfsg.P4-12.3+deb9u12.</p>

<p>Nous vous recommandons de mettre à jour vos paquets bind9.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de bind9, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/bind9">https://security-tracker.debian.org/tracker/bind9</a></p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur : <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2955-2.data"
# $Id: $
