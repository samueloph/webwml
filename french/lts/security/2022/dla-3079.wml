#use wml::debian::translation-check translation="d70f5cb91a36942166533ac04d23ccabf396e9ac" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux vulnérabilités de sécurité ont été découvertes dans Jetty, un moteur
de servlet Java et serveur web.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2047">CVE-2022-2047</a>

<p>Dans Eclipse Jetty, l'analyse du segment <q>authority</q> d'un schéma
d'URI HTTP, la classe HttpURI de Jetty, détecte incorrectement une entrée
non valable comme nom d'hôte. Cela peut conduire à des échecs dans un
scénario de mandataire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2048">CVE-2022-2048</a>

<p>Dans l'implémentation du serveur HTTP/2 Eclipse Jetty, lors de la
rencontre d'une requête HTTP/2 non valable, le traitement de l'erreur
contient un bogue qui peut finir par ne pas nettoyer correctement les
connexions actives et les ressources associées. Cela peut conduire à un
scénario de déni de service où il reste insuffisamment de ressources pour
traiter les requêtes correctes.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés
dans la version 9.4.16-0+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets jetty9.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de jetty9, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/jetty9">\
https://security-tracker.debian.org/tracker/jetty9</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p></li>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3079.data"
# $Id: $
