#use wml::debian::translation-check translation="8b80b27d2b93f27883834f7d984955beee3c0772" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>La mise à jour de sécurité annoncée sous le nom de DLA-2962-1 présentait
une régression due à une erreur dans le rétroportage du correctif du <a
href="https://security-tracker.debian.org/tracker/CVE-2022-23608">CVE-2022-23608</a>.
Les paquets mis à jour de pjproject sont désormais disponibles.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
2.5.5~dfsg-6+deb9u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets pjproject.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de pjproject, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/pjproject">
https://security-tracker.debian.org/tracker/pjproject</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2962-2.data"
# $Id: $
