#use wml::debian::translation-check translation="dd9e9205ab73d54534159c6ac523dd79f6b7e82b" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une vulnérabilité de dissimulation éventuelle de requête HTTP dans WEBrick
a été signalée.</p>

<p>WEBrick (fourni avec jruby) était trop indulgent avec un en-tête
Transfer-Encoding non valable. Cela pouvait conduire à une interprétation
incompatible entre WEBrick et certains serveurs mandataires HTTP, et pouvait
permettre à un attaquant de passer subrepticement une requête.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 1.7.26-1+deb9u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets jruby.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de jruby, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/jruby">https://security-tracker.debian.org/tracker/jruby</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2392.data"
# $Id: $
