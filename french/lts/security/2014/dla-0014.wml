#use wml::debian::translation-check translation="0489e1b952f47155cfd52286f4b52319011dbc06" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans phpMyAdmin, un outil
pour administrer MySQL par le web. Le projet « Common Vulnerabilities and
Exposures » (CVE) identifie les problèmes suivants.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2013-3239">CVE-2013-3239</a>

<p>Des utilisateurs authentifiés pourraient exécuter du code arbitraire,
lorsque un répertoire SaveDir est configuré et que le serveur HTTP Apache a
le module mod_mime activé, en employant des noms de fichiers avec une
extension double.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2013-4995">CVE-2013-4995</a>

<p>Des utilisateurs authentifiés pourraient injecter un script web
arbitraire ou du HTML à l'aide d'une requête SQL contrefaite.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2013-4996">CVE-2013-4996</a>

<p>Une vulnérabilité de script intersite était possible à l'aide d'une URL
de logo contrefaite dans le panneau de navigation ou d'une entrée
contrefaite dans la liste des serveurs mandataires de confiance (« List of
Trusted Proxies »). </p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2013-5003">CVE-2013-5003</a>

<p>Des utilisateurs authentifiés pourraient exécuter des commandes SQL
arbitraires en tant qu'« utilisateur de contrôle » de phpMyAdmin à l'aide
du paramètre échelle de pmd_pdf export.</p></li>

</ul>

<p>Pour Debian 6 <q>Squeeze</q>, ces problèmes ont été corrigés dans la
version 4:3.3.7-8 de phpmyadmin.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2014/dla-0014.data"
# $Id: $
