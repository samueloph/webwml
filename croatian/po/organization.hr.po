msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2006-08-12 16:56+0200\n"
"Last-Translator: Josip Rodin\n"
"Language-Team: Croatian\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr ""

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr ""

#. One male delegate
#. Pronoun tags with delegate combinations
#: ../../english/intro/organization.data:18
#: ../../english/intro/organization.data:22
msgid "<void id=\"male\"/>delegate"
msgstr ""

#. One female delegate
#: ../../english/intro/organization.data:20
#: ../../english/intro/organization.data:23
msgid "<void id=\"female\"/>delegate"
msgstr ""

#: ../../english/intro/organization.data:22
#: ../../english/intro/organization.data:25
msgid "<void id=\"he_him\"/>he/him"
msgstr ""

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:26
msgid "<void id=\"she_her\"/>she/her"
msgstr ""

#: ../../english/intro/organization.data:24
msgid "<void id=\"gender_neutral\"/>delegate"
msgstr ""

#: ../../english/intro/organization.data:24
#: ../../english/intro/organization.data:27
msgid "<void id=\"they_them\"/>they/them"
msgstr ""

#: ../../english/intro/organization.data:30
#: ../../english/intro/organization.data:32
msgid "current"
msgstr "trenutni"

#: ../../english/intro/organization.data:34
#: ../../english/intro/organization.data:36
msgid "member"
msgstr "član"

#: ../../english/intro/organization.data:39
msgid "manager"
msgstr "upravitelj"

#: ../../english/intro/organization.data:41
msgid "SRM"
msgstr ""

#: ../../english/intro/organization.data:41
#, fuzzy
msgid "Stable Release Manager"
msgstr "Nadgledanje izdanja"

#: ../../english/intro/organization.data:43
msgid "wizard"
msgstr "čarobnjak"

#. we only use the chair tag once, for techctte, I wonder why it's here.
#: ../../english/intro/organization.data:45
#, fuzzy
msgid "chair"
msgstr "predsjedavatelj"

#: ../../english/intro/organization.data:48
msgid "assistant"
msgstr "pomoćnik"

#: ../../english/intro/organization.data:50
msgid "secretary"
msgstr "tajnik"

#: ../../english/intro/organization.data:52
msgid "representative"
msgstr ""

#: ../../english/intro/organization.data:54
msgid "role"
msgstr ""

#: ../../english/intro/organization.data:62
msgid ""
"In the following list, <q>current</q> is used for positions that are\n"
"transitional (elected or appointed with a certain expiration date)."
msgstr ""

#: ../../english/intro/organization.data:70
#: ../../english/intro/organization.data:82
msgid "Officers"
msgstr "Službenici"

#: ../../english/intro/organization.data:71
#: ../../english/intro/organization.data:108
msgid "Distribution"
msgstr "Distribucija"

#: ../../english/intro/organization.data:72
#: ../../english/intro/organization.data:203
msgid "Communication and Outreach"
msgstr ""

#: ../../english/intro/organization.data:74
#: ../../english/intro/organization.data:206
msgid "Data Protection team"
msgstr ""

#: ../../english/intro/organization.data:75
#: ../../english/intro/organization.data:211
#, fuzzy
msgid "Publicity team"
msgstr "Publicitet"

#: ../../english/intro/organization.data:77
#: ../../english/intro/organization.data:284
msgid "Membership in other organizations"
msgstr ""

#: ../../english/intro/organization.data:78
#: ../../english/intro/organization.data:312
msgid "Support and Infrastructure"
msgstr "Podrška i infrastruktura"

#: ../../english/intro/organization.data:85
msgid "Leader"
msgstr "Vođa"

#: ../../english/intro/organization.data:87
msgid "Technical Committee"
msgstr "Tehnički odbor"

#: ../../english/intro/organization.data:103
msgid "Secretary"
msgstr "Tajnik"

#: ../../english/intro/organization.data:111
msgid "Development Projects"
msgstr "Razvojni projekti"

#: ../../english/intro/organization.data:112
msgid "FTP Archives"
msgstr "FTP arhive"

#: ../../english/intro/organization.data:114
#, fuzzy
msgid "FTP Masters"
msgstr "FTP Master"

#: ../../english/intro/organization.data:120
msgid "FTP Assistants"
msgstr "FTP Assistants"

#: ../../english/intro/organization.data:126
msgid "FTP Wizards"
msgstr ""

#: ../../english/intro/organization.data:130
msgid "Backports"
msgstr ""

#: ../../english/intro/organization.data:132
msgid "Backports Team"
msgstr ""

#: ../../english/intro/organization.data:136
msgid "Release Management"
msgstr "Nadgledanje izdanja"

#: ../../english/intro/organization.data:138
msgid "Release Team"
msgstr "Tim za izdanja"

#: ../../english/intro/organization.data:147
msgid "Quality Assurance"
msgstr "Osiguranje kvalitete"

#: ../../english/intro/organization.data:148
msgid "Installation System Team"
msgstr "Tim za instalacijski sustav"

#: ../../english/intro/organization.data:149
msgid "Debian Live Team"
msgstr ""

#: ../../english/intro/organization.data:150
msgid "Release Notes"
msgstr "Napomene izdanja"

#: ../../english/intro/organization.data:152
#, fuzzy
#| msgid "CD Images"
msgid "CD/DVD/USB Images"
msgstr "CD snimke"

#: ../../english/intro/organization.data:154
msgid "Production"
msgstr "Izrada"

#: ../../english/intro/organization.data:161
msgid "Testing"
msgstr "Testiranje"

#: ../../english/intro/organization.data:163
msgid "Cloud Team"
msgstr ""

#: ../../english/intro/organization.data:167
#, fuzzy
msgid "Autobuilding infrastructure"
msgstr "Podrška i infrastruktura"

#: ../../english/intro/organization.data:169
msgid "Wanna-build team"
msgstr ""

#: ../../english/intro/organization.data:176
#, fuzzy
msgid "Buildd administration"
msgstr "Administracija buildda"

#: ../../english/intro/organization.data:193
msgid "Documentation"
msgstr "Dokumentacija"

#: ../../english/intro/organization.data:198
msgid "Work-Needing and Prospective Packages list"
msgstr "Popis paketa koji zahtijevaju rad i paketi u prospektu"

#: ../../english/intro/organization.data:214
msgid "Press Contact"
msgstr "Kontakt za tisak"

#: ../../english/intro/organization.data:216
msgid "Web Pages"
msgstr "WWW stranice"

#: ../../english/intro/organization.data:228
msgid "Planet Debian"
msgstr "Planet Debian"

#: ../../english/intro/organization.data:233
msgid "Outreach"
msgstr ""

#: ../../english/intro/organization.data:238
msgid "Debian Women Project"
msgstr "Debian Women projekt"

#: ../../english/intro/organization.data:246
msgid "Community"
msgstr ""

#: ../../english/intro/organization.data:255
msgid ""
"To send a private message to all the members of the Community Team, use the "
"GPG key <a href=\"community-team-pubkey.txt"
"\">817DAE61E2FE4CA28E1B7762A89C4D0527C4C869</a>."
msgstr ""

#: ../../english/intro/organization.data:257
msgid "Events"
msgstr "Događaji"

#: ../../english/intro/organization.data:264
#, fuzzy
msgid "DebConf Committee"
msgstr "Tehnički odbor"

#: ../../english/intro/organization.data:271
msgid "Partner Program"
msgstr "Program partnera"

#: ../../english/intro/organization.data:275
msgid "Hardware Donations Coordination"
msgstr "Koordinacija donacija hardvera"

#: ../../english/intro/organization.data:290
msgid "GNOME Foundation"
msgstr ""

#: ../../english/intro/organization.data:292
msgid "Linux Professional Institute"
msgstr ""

#: ../../english/intro/organization.data:294
msgid "Linux Magazine"
msgstr ""

#: ../../english/intro/organization.data:296
msgid "Linux Standards Base"
msgstr ""

#: ../../english/intro/organization.data:298
msgid "Free Standards Group"
msgstr ""

#: ../../english/intro/organization.data:299
msgid "SchoolForge"
msgstr ""

#: ../../english/intro/organization.data:302
msgid ""
"OASIS: Organization\n"
"      for the Advancement of Structured Information Standards"
msgstr ""

#: ../../english/intro/organization.data:305
msgid ""
"OVAL: Open Vulnerability\n"
"      Assessment Language"
msgstr ""

#: ../../english/intro/organization.data:308
msgid "Open Source Initiative"
msgstr ""

#: ../../english/intro/organization.data:315
msgid "Bug Tracking System"
msgstr "Sustav praćenja bugova"

#: ../../english/intro/organization.data:320
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr "Administracija mailing lista i arhiva mailing lista"

#: ../../english/intro/organization.data:329
#, fuzzy
msgid "New Members Front Desk"
msgstr "Front-desk za nove održavatelje"

#: ../../english/intro/organization.data:335
msgid "Debian Account Managers"
msgstr "Upravitelji korisničkih računa razvijatelja"

#: ../../english/intro/organization.data:339
msgid ""
"To send a private message to all DAMs, use the GPG key "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""

#: ../../english/intro/organization.data:340
msgid "Keyring Maintainers (PGP and GPG)"
msgstr "Održavatelji prstena ključeva (PGP i GPG)"

#: ../../english/intro/organization.data:344
msgid "Security Team"
msgstr "Sigurnosni tim"

#: ../../english/intro/organization.data:355
msgid "Policy"
msgstr "Policy"

#: ../../english/intro/organization.data:358
msgid "System Administration"
msgstr "Administracija sustava"

#: ../../english/intro/organization.data:359
msgid ""
"This is the address to use when encountering problems on one of Debian's "
"machines, including password problems or you need a package installed."
msgstr ""
"Ovo je adresa koju treba koristiti kada imate problema na jednom od "
"Debianovih strojeva, uključujući probleme sa lozinkama i potrebe za "
"instaliranjem paketa."

#: ../../english/intro/organization.data:369
msgid ""
"If you have hardware problems with Debian machines, please see <a href="
"\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it should "
"contain per-machine administrator information."
msgstr ""
"Ako imate hardverskih problema s Debian strojevima, molimo pogledajte "
"stranicu <a href=\"https://db.debian.org/machines.cgi\">Debian strojeva</a>, "
"koja bi trebala sadržavati informacije o administratorima pojedinih strojeva."

#: ../../english/intro/organization.data:370
msgid "LDAP Developer Directory Administrator"
msgstr "Administrator LDAP direktorija razvijatelja"

#: ../../english/intro/organization.data:371
msgid "Mirrors"
msgstr "Mirrori"

#: ../../english/intro/organization.data:378
msgid "DNS Maintainer"
msgstr "Održavatelj DNS-a"

#: ../../english/intro/organization.data:379
msgid "Package Tracking System"
msgstr "Sustav praćenja paketa"

#: ../../english/intro/organization.data:381
msgid "Treasurer"
msgstr ""

#: ../../english/intro/organization.data:388
msgid ""
"<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr ""

#: ../../english/intro/organization.data:392
#, fuzzy
msgid "Salsa administrators"
msgstr "Administratori Aliotha"

#~ msgid "Individual Packages"
#~ msgstr "Pojedini paketi"

#~ msgid "Testing Security Team"
#~ msgstr "Sigurnosni tim za <q>testing</q>"

#~ msgid "Security Audit Project"
#~ msgstr "Projekt Security Audit"

#~ msgid ""
#~ "This is not yet an official Debian internal project but it has announced "
#~ "the intention to be integrated."
#~ msgstr ""
#~ "Ovo još nije službeni interni projekt u Debianu ali su najavili namjeru "
#~ "integracije."

#~ msgid "Debian for non-profit organisations"
#~ msgstr "Debian za neprofitne organizacije"

#~ msgid "The Universal Operating System as your Desktop"
#~ msgstr "Univerzalni operativni sustav kao vaš desktop"

#~ msgid "Accountant"
#~ msgstr "Knjigovođa"

#~ msgid "Key Signing Coordination"
#~ msgstr "Koordinacija potpisivanja ključeva"

#~ msgid ""
#~ "Names of individual buildd's admins can also be found on <a href=\"http://"
#~ "www.buildd.net\">http://www.buildd.net</a>.  Choose an architecture and a "
#~ "distribution to see the available buildd's and their admins."
#~ msgstr ""
#~ "Imena pojedinih administratora builddova se mogu naći i na <a href="
#~ "\"http://www.buildd.net\">http://www.buildd.net</a>. Odaberite "
#~ "arhitekturu i distribuciju kako bi vidjeli dostupne builddove i njihove "
#~ "administratore."

#~ msgid ""
#~ "The admins responsible for buildd's for a particular arch can be reached "
#~ "at <genericemail arch@buildd.debian.org>, for example <genericemail "
#~ "i386@buildd.debian.org>."
#~ msgstr ""
#~ "Administratori koji su odgovorni za builddove za pojedinu arhitekturu se "
#~ "mogu dobiti na <genericemail arch@buildd.debian.org>, npr. <genericemail "
#~ "i386@buildd.debian.org>."

#~ msgid "Marketing Team"
#~ msgstr "Marketing tim"

#~ msgid "Handhelds"
#~ msgstr "Ručna računala (dlanovnici)"

#~ msgid "APT Team"
#~ msgstr "APT tim"

#~ msgid "Vendors"
#~ msgstr "Distributeri"

#~ msgid "Release Team for ``stable''"
#~ msgstr "Tim za izdanje <q>stable</q>"

#~ msgid "Custom Debian Distributions"
#~ msgstr "Prilagođene Debian distribucije"

#, fuzzy
#~| msgid "Debian Maintainer Keyring Team"
#~ msgid "Debian Maintainer (DM) Keyring Maintainers"
#~ msgstr "Debian Maintainer keyring tim"

#~ msgid "Publicity"
#~ msgstr "Publicitet"

#~ msgid "Auditor"
#~ msgstr "Revizor"

#, fuzzy
#~| msgid "Installation System Team"
#~ msgid "Live System Team"
#~ msgstr "Tim za instalacijski sustav"

#~ msgid "Alioth administrators"
#~ msgstr "Administratori Aliotha"

#, fuzzy
#~| msgid "Debian for education"
#~ msgid "Debian for astronomy"
#~ msgstr "Debian za obrazovanje"

#, fuzzy
#~| msgid "Debian for medical practice and research"
#~ msgid "Debian for science and related research"
#~ msgstr "Debian za medicinsku praksu i istraživanje"

#~ msgid "Debian for people with disabilities"
#~ msgstr "Debian za ljude sa invaliditetom"

#~ msgid "Debian in legal offices"
#~ msgstr "Debian u pravnim uredima"

#~ msgid "Debian for education"
#~ msgstr "Debian za obrazovanje"

#~ msgid "Debian for medical practice and research"
#~ msgstr "Debian za medicinsku praksu i istraživanje"

#~ msgid "Debian for children from 1 to 99"
#~ msgstr "Debian za djecu od 1 do 99"

#~ msgid "User support"
#~ msgstr "Podrška korisnicima"

#~ msgid "Embedded systems"
#~ msgstr "<i>Embedded</i> sustavi"

#~ msgid "Firewalls"
#~ msgstr "Firewalli"

#~ msgid "Laptops"
#~ msgstr "Laptopi"

#~ msgid "Special Configurations"
#~ msgstr "Posebne konfiguracije"

#~ msgid "Ports"
#~ msgstr "Portovi"

#~ msgid "CD Vendors Page"
#~ msgstr "Stranica za distributere CD-a"

#~ msgid "Consultants Page"
#~ msgstr "Stranica za konzultante"
