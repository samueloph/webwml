#use wml::debian::template title="Hur www.debian.org skapas" MAINPAGE="true"
#use wml::debian::translation-check translation="835e2e2ec1141439d03f50e38dba3a48eaaf6e81"
#use wml::debian::toc

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
<li><a href="#look">Utseende &amp; känsla</a></li>
<li><a href="#sources">Sources</a></li>
<li><a href="#scripts">Scripts</a></li>
<li><a href="#generate">Generating the Website</a></li>
<li><a href="#help">How to help</a></li>
<li><a href="#faq">Hur du inte hjälper till... (FAQ)</a></li>
</ul>

<h2><a id="look">Utseende &amp; känsla</a></h2>

<p>Debians webbplats är en samling av mappar och filer som placerats
i <code>/org/www.debian.org/www</code> på <em>www-master.debian.org</em>.
De flesta sidorna är statiska HTML-filer. De innehåller inte dynamiska element
som CGI eller PHP-skript, eftersom webbplatsen speglas.
</p>

<p>
Debians webbplats använder Website Meta Language
(<a href="https://packages.debian.org/unstable/web/wml">WML</a>) för att generera HTML-sidor
inklusive sidhuvuden och sidfötter, innehållsförteckningar, osv.
Även om en <code>.wml</code>-fil kan se ut som HTML vid en första anblick är HTML bara
en av typerna av extra information som kan användas i WML. Du kan även använda dig av
Perl-kod i en sida och tack vare detta göra nästan vad som helst.
</p>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Våra webbsidor följer för närvarane standarden <a href="http://www.w3.org/TR/html4/">HTML 4.01 Strict</a>.</p>
</aside>

<p>
Efter att WML är klar med att köra sina olika filter över en fil är
det slutgiltiga resultatet äkta HTML.
Observera dock att WML endast kontrollerar (och ibland automagiskt
korrigerar) grundläggande riktighet i din HTML-kod.
Du bör installera
<a href="https://packages.debian.org/unstable/web/weblint">weblint</a>
och/eller
<a href="https://packages.debian.org/unstable/web/tidy">tidy</a>
för att kontrollera din HTML-kod.

</p>

<p>
Alla som arbetar med ett stort antal sidor borde installera WML så att
de kan testa och vara säkra på att resultatet är det de önskar.
Om du använder Debian kan du väldigt enkelt installera
<code>wml</code>-paketet.
Läs sidorna om att <a href="using_wml">använda WML</a> för ytterligare
information.

</p>

<h2><a id="sources">Källkod</a></h2>

<p>
Vi använder oss av Git för att lagra Debians webbplats källkod.
Versionshanteringssystemet tillåter oss att spåra alla förändringar och vi kan
se vem som skickade in vad och när, och till och med varför. Git erbjuder
ett säkert sätt att kontrollera samtidig redigering av källkodsfiler av flera
författare - en avgörande uppgift för Debians webbteam eftersom vi har
många medlemmar.
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="using_git">Läs mer om Git</a></button></p>

<p>
Här följer bakgrundsinformation om hur källkoden struktureras:
</p>

<ul>
   <li>Den översta katalogen i Git-förrådet, webwml, innehåller kataloger som
      namnges efter språket webbplatsen översätts till, två makefiler och flera
      skript.
      Översättningskatalogens namn bör vara på engelska och skrivas med små
      bokstäver (t.ex <code>german</code>, inte <code>Deutsch</code>).
   </li>

   <li>Den viktigaste av de två makefilerna är <code>Makefile.common</code>,
      vilken, som namnet antyder, innehåller några gemensamma regler som
      appliceras genom att inkludera denna i fil i andra makefiler.
   </li>

   <li>Alla underkataloger för de olika språken innehåller också Makefiler,
      olika <code>.wml</code>-källkodsfiler, och ytterligare underkataoger.
      Alla fil- och mappnamn följer ett särskilt mönster, så att alla länkar
      fungerar för de översatta filerna.
      Några mappar innehåller också <code>.wmlrc</code>-konfigurationsfiler med
      ytterligare kommandon och inställningar för WML.</li>
   
   <li>Mappen <code>webwml/english/template</code> innhåller speciella WML-filer
      som fungerar som mallar. Dessa kan refereras från alla andra filer genom
      <code>#use</code>-kommandot.</li>
</ul>


<p>
Vänligen notera: För att ändringar i mallarna skall propagera till filerna som använder
dem har makefilerna beroenden på dem.
Eftersom en stor majoritet av filerna använder <code>template</code>-mallen,
den allmänna beroendet, så de använder följande rad på toppen av filen:


<p>
<code>#use wml::debian::template</code>
</p>

<p>
Det finns självklart undantag till denna regel.
</p>

<h2><a id="scripts">Skript</a></h2>

<p>
Skripten är huvudsakligen skrivna i skalspråket eller Perl.
En del av dem är fristående medan några är integrerade i WML-källfilerna.
</p>

<ul>
  <li><a href="https://salsa.debian.org/webmaster-team/cron.git">webmaster-team/cron</a>:
Detta Git-förråd innehåller alla skript som används för att uppdatera Debians
webbplats, t.ex. källkoden för ombyggnadsskripten för <code>www-master</code>.</li>
  <li><a href="https://salsa.debian.org/webmaster-team/packages">webmaster-team/packages</a>:
Detta Git-förråd innehåller källkoden för ombyggnadsskriptenen för <code>packages.debian.org</code>.</li>
</ul>

<h2><a id="generate">Generera webbplatsen</a></h2>

<p>
WML, mallar, skal- eller Perlskript är alltihop ingredienser som du kommer att behöva
för att generera Debians webbplats:
</p>

<ul>
  <li>Majoriteten genereras med hjälp av WML (från <a href="$(DEVEL)/website/using_git">Gitförrådet</a>).</li>
  
  <li>Dokumnetationen genereras med antingen DocBook XML (<a href="$(DOC)/vcs"><q>ddp</q> Gitförråd</a>)
  <li>The documentation is generated with either DocBook XML (<a href="$(DOC)/vcs"><q>ddp</q> Git repository</a>)
eller med <a href="#scripts">cron-skript</a> från motsvarande Debianpaket.</li>
  <li>Några delar av webbplatsen genereras med skript från andra källor,
  exempelvis sidorna för sändlistornas prenumeration / säga upp prenumeration.</li>
</ul>

<p>
En automatisk uppdatering (från Gitförrådet eller andra käller till webbträdet) körs sex gånger per dag.
Utöver detta kör vi följande kontroller på hela webbplatsen:
</p>

<ul>
  <li><a href="https://www-master.debian.org/build-logs/urlcheck/">URL-kontroller</a>
  <li><a href="https://www-master.debian.org/build-logs/validate/">wdg-html-validator</a>
  <li><a href="https://www-master.debian.org/build-logs/tidy/">tidy</a>
</ul>

<p>
De aktuella byggloggarna för webbplatsen kan hittas på
<url "https://www-master.debian.org/build-logs/">.
</p>

<p>Om du vill bidra till webbplatsen, redigera <strong>inte</strong> filer
i <code>www/</code>-mappen direkt eller lägg till nya filer. Kontakta istället
<a href="mailto:webmaster@debian.org">webbteamet</a> först.
</p>

<aside>
<p><span class="fas fa-cogs fa-3x"></span> Tekniskt:
Alla filer och mappar ägs av <code>debwww</code>-gruppen som har skrivrättigheter.
På det sättet kan webbgruppen redigera innehåll i webbmapparna.
<code>2775</code>-läget på mapparna betyder att alla filer som skapas under den
katalogen kommer att ärva gruppen (<code>debwww</code>). Medlemmar i gruppen
förväntas att sätta <code>umask 002</code> så att filer skapas med
gruppens skrivrättigheter.</p>
</aside>

<h2><a id="help">Hur du hjälper till</a></h2>

<p>
Vi uppmuntrar alla att hjälpa till med webbplatsen. Om du har värdefull
information relaterad till Debian som du tycker saknas, var vänlig
<a href="mailto:debian-www@lists.debian.org">hör av dig</a> - Vi kommmer att
se till att det inkluderas.
Ta även en titt på ovan nämnda <a
href="https://www-master.debian.org/build-logs/">byggloggar</a> och kolla om
du har några förslag på hur man kan fixa något problem.
</p>

<p>
Vi söker även efter folk som kan hjälpa till med designen
(grafik, layout, osv.). Om du talar engelska flytande, var vänlig
korrekturläs våra sidor och
<a href="mailto:debian-www@lists.debian.org">rapporera</a> fel.
Om du talar ett annat språk kan du vilja hjälpa till att översätta existerande
sidor eller hjälpa till med att rätta fel i redan översatta sidor.
I båda fall, var vänlig ta en titt på listan för

<a href="translation_coordinators">översättningssamordnare</a> och kontakta
den ansvarige personen. För ytterligare information, vänligen ta en
titt på vår <a href="translating">sida för översättare</a>.
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="todo">Läs vår Att göra-lista</a></button></p>


<aside class="light">
  <span class="fa fa-question fa-5x"></span>
</aside>

<h2><a id="faq">Hur du inte hjälper till... (FAQ)</a></h2>

<p>
<strong>[F] Jag vill lägga till <em>tuff webbfunktion</em> på Debians webbplats, får jag det?</strong>
</p>

<p>
[S] Nej. Vi vill att www.debian.org ska vara så åtkomlig som möjligt., so
</p>

<ul>
    <li>inga webbläsarspecifika "tillägg",
    <li>inget beroende endast på bilder.
        Bilder kan användas för att klargöra, men informationen på
        www.debian.org måste vara tillgänglig via en textbaserad webbläsare
        såsom Lynx.

</ul>

<p>
<strong>[F] Jag har en bra idé som jag vill bidra med. Skulle ni kunna <em>foo</em> eller <em>bar</em> på
www.debian.org's HTTP-server?</strong>
</p>

<p>
[S] Nej. Vi vill göra det så lätt som möjligt för administratörer att
spegla www.debian.org, så inga speciella HTTPD-funktioner, är ni snälla.
Nej, inte ens SSI (Server Side Includes).
Ett undantag har gjorts för innehållsförhandling, eftersom det är det enda
robusta sättet att tillhandahålla flera språk på.

</p>
