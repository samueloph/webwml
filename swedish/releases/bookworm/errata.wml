#use wml::debian::template title="Debian 12 -- Errata" BARETITLE=true
#use wml::debian::toc
#use wml::debian::translation-check translation="a5d0ace4d5a31020a8e5aa86a3b29c8089f7574b"

#include "$(ENGLISHDIR)/releases/info"

<toc-display/>


# <toc-add-entry name="known_probs">Kända problem</toc-add-entry>
<toc-add-entry name="security">Säkerhetsproblem</toc-add-entry>

<p>Debians säkerhetsgrupp ger ut uppdateringar till paket i den stabila utgåvan
där de har identifierat problem relaterade till säkerhet. Vänligen se
<a href="$(HOME)/security/">säkerhetssidorna</a> för information om
potentiella säkerhetproblem som har identifierats i <q>Bookworm</q>.</p>

<p>Om du använder APT, lägg till följande rad i <tt>/etc/apt/sources.list</tt>
för att få åtkomst till de senaste säkerhetsuppdateringarna:</p>

<pre>
  deb http://security.debian.org/ bookworm-security main contrib non-free-firmware non-free
</pre>

<p>Kör <kbd>apt update</kbd> efter detta, följt av
<kbd>apt upgrade</kbd>.</p>


<toc-add-entry name="pointrelease">Punktutgåvor</toc-add-entry>

<p>Ibland, när det gäller kritiska problem eller säkerhetsuppdateringar,
uppdateras utgåvedistributionen. Generellt indikeras detta som punktutgåvor.
</p>


<ifeq <current_release_bookworm> 12.0 "


<p>Det finns inga punktutgåvor för Debian 12 ännu.</p>" "

<p>Se <a
href="http://http.us.debian.org/debian/dists/bookworm/ChangeLog">\
förändringsloggen</a>
för detaljer om förändringar mellan 12 och <current_release_bookworm/>.</p>"/>


<p>Rättningar till den utgivna stabila utgåvan går ofta genom en
utökad testningsperiod innan de accepteras i arkivet. Dock, så finns dessa
rättningar tillgängliga i mappen
<a href="http://ftp.debian.org/debian/dists/bookworm-proposed-updates/">\
dists/bookworm-proposed-updates</a> i alla Debianarkivspeglingar.</p>

<p>Om du använder APT för att uppdatera dina paket kan du installera de
föreslagna uppdateringarna genom att lägga till följande rad i
<tt>/etc/apt/sources.list</tt>:</p>

<pre>
  \# föreslagna tillägg för en punktutgåva till Debian 12
  deb http://deb.debian.org/debian bookworm-proposed-updates main contrib non-free-firmware non-free
</pre>

<p>Kör <kbd>apt update</kbd> efter detta, följt av
<kbd>apt upgrade</kbd>.</p>

<toc-add-entry name="installer">Installationssystem</toc-add-entry>

<p>
För mer information om kända problem och uppdateringar till
installationssystemet, se
<a href="debian-installer/">debian-installer</a>-sidan.
</p>
