#use wml::debian::template title="Vad betyder &rdquo;fritt&rdquo;?" NOHEADER="yes"
#use wml::debian::translation-check translation="2de158b0e259c30eefd06baf983e4930de3d7412"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#freesoftware">Fritt som i...?</a></li>
    <li><a href="#licenses">Mjukvarulicenser</a></li>
    <li><a href="#choose">Hur väljer man licens?</a></li>
  </ul>
</div>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> I februari 1998 jobbade en grupp
människor för att ersätta termen <a href="https://www.gnu.org/philosophy/free-sw">Fri
mjukvara (eng. Free Software</a> med <a href="https://opensource.org/docs/definition.html">mjukvara
med öppen källkod (eng. Open Source Software)</a>. Terminologidebatten
reflekterar den underliggande filosofiska skillnaden, men de praktiska kraven så
väl som andra saker som diskuteras på denna webbplats är i grunden samma för
fri mjukvara och mjukvara med öppen källkod.</p>
</aside>


<h2><a id="freesoftware">Fritt som i...?</a></h2>

<p>
Många som är nya inom detta ämne blir förvirrade på grund av ordet
"fritt". Det används inte som dom förväntar sig - "fritt" har för dom
betydelsen "utan kostnad". Om du tittar i en engelsk ordbok, visar den
nästan tjugo olika betydelser för ordet "fritt", och endast en av dessa
är "utan kostnad". Resten av orden refererar till "frihet" och
"utan begränsningar". Så när vi talar om fri mjukvara så menar vi frihet,
inte betalning. (Problematiken är värre på engelska då ordet är "free" som
på svenska kan översättas till både fri och gratis).
</p>

<p>
Mjukvara som beskrivs som fri, men endast i betydelsen att du inte behöver
betala för den, är knappast fri alls. Du kan vara förbjuden att skicka
den vidare, och du är med största sannolikhet inte tillåten att modifiera
den. Mjukvara som licensieras utan kostnad är ofta ett verktyg i en
marknadskampanj för att främja en relaterad produkt eller för att
driva en konkurrent till konkurs. Det finns inga garantier att den kommer
att hållas gratis en längre tid.
</p>

<p>
För den icke initierade är mjukvara antingen gratis (fri) eller inte. I
verkliga livet är det betydligt mer komplicerat än så. För att förstå
vad folk menar när dom talar om fri mjukvara, låt oss ta en liten
omväg och titta på licensvärlden.
</p>

<h2><a id="licenses">Mjukvarulicenser</a></h2>

<p>
Copyright är en metod att skydda skaparnas rättigheter i vissa typer
av verk. I de flesta länder får nyskriven mjukvara automatiskt copyright.
En licens är skaparens sätt att tillåta andra att använda hans eller
hennes skapelse (mjukvara i detta fall), på sätt som är acceptabla för
dem. Det är upp till skaparen att inkludera en licens som deklarerar hur
en mjukvara får användas.
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="https://www.copyright.gov/">Läs mer om copyright</a></button></p>

<p>
Självklart så använder man olika licenser beroende på aktuella omständigheter.
Mjukvaruföretag söker efter sätta att skydda sina tillgångar, så dom
släpper ofta endast kompilerad kod som inte läsbart av människor. Det kan
även lägga på begränsningar på hur mjukvaran får användas. Författare av
fri mjukvara å andra sidan söker ofta efter andra regler, ibland till och
med en kombination av följande punkter:
</p>

<ul>
  <li>Det är inte tillåtet att använda dess kod i proprietär mjukvara. Eftersom
      dom släpper sin kod så att alla kan använda den, så vill dom inte att
      folk stjäl den. I detta fall ses användning av koden som ett förtroende:
      du får använda koden, så länge som du kör efter samma regler.</li>
  <li>Skaparens identitet måste skyddas. Folk tar stor öra i sina verk och
      vill inte att andra ska ta bort deras namn från författarlistan eller
      till och med hävda att dom skrev det själva.</li>
  <li>Källkod måste distribueras. Ett stort problem med proprietär
      mjukvara är att du inte kan rätta buggar eller anpassa koden, eftersom
      källkoden inte finns tillgänglig. Utöver detta kan en mjukvaruutvecklare
      välja att sluta att stödja användares hårdvara. Distribution av källkod,
      så som de flesta fri mjukvarulicenser föreskriver skyddar användarna
      genom att tillåta dom att anpassa mjukvaran för att justera den för
      deras behov.</li>
  <li>Alla verk som inkluderar någon del av skaparens verk (kallas även
      "härledda verk" i copyrightdiskussioner) måste använda samma licens.</li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Tre av de mest använda licenserna för fri mjukvara är <a href="https://www.gnu.org/copyleft/gpl.html">GNU General Public License (GPL)</a>, <a href="https://opensource.org/licenses/artistic-license.php">Artistic License</a>, och <a href="https://opensource.org/licenses/BSD-3-Clause">BSD Style License</a>.
</aside>

<h2><a id="choose">Hur väljer man licens?</a></h2>

<p>
Ibland skriver folk sina egna licenser, vilket kan vara problematiskt,
så detta ses ogillande på i gemenskapen för fri mjukvara. Allt för ofta
är formuleringarna tvetydiga, eller så kan folk skapa villkor som strider
mot varandra. Att skapa en licens som håller i domstol är till och med
svårare. Lyckligtvis nog finns det ett antal licenser för fri mjukvara att
välja på. Dom har följande gemensamt:
</p>

<ul>
  <li>Användare kan installera programvara på så många maskiner som dom vill.</li>
  <li>En obegränsad mängd personer kan använda programvaran samtidigt.</li>
  <li>Användare kan skapa hur många kopior av mjukvaran som dom vill eller behöver och också ge dessa kopior till andra användare (fri eller öppen distribution).</li>
  <li>Det finns inga begränsningar på hur du får modifiera mjukvaran (förutom när det gäller att behålla vissa notiser).</li>
  <li>Användare kan inte endast distribuera mjukvaran, de får även sälja den.</li>
</ul>

<p>
Speciellt den sista punkten, som tillåter folk att sälja mjukvara kan
verka gå emot hela idéen med fri mjukvara, men den är faktiskt en av dess
styrkor. Eftersom licensen tillåter fri distribution, när någon får tag på en
kopia kan han eller hon även distribuera det. Folk kan till och med försöka
sälja den.
</p>


<p>
Medan fri mjukvara inte är helt fritt från begränsningar, ger det
användarna flexibiliteten att göra det de behöver för att få jobbet
gjort. På samma gång skyddas skapares rättigheter - det är frihet.
Debianprojektet och dess medlemmar känner starkt för fri mjukvara. Vi har
sammanställt <a
href="../social_contract#guidelines">Debians riktlinjer för fri mjukvara
(Debian Free Software Guidelines - DFSG)</a> för att få en rimlig definition
av vad som är fri mjukvara enligt vår åsikt. Endast programvara vars licens
lyder DFSG tillåts vara med i Debians distribution.
</p>
