#use wml::debian::translation-check translation="9f4968f1d9a97ff25da0334028d0e74ab7c96c32" mindelta="1"
<define-tag description>säkerhetsuppdatering</define-tag>
<define-tag moreinfo>
<p>Flera sårbarheter har upptäckts i webbmotorn webkit2gtk:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8644">CVE-2019-8644</a>

    <p>G. Geshev upptäckte minneskorrumperingsproblem som kan leda till
    godtycklig kodexekvering.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8649">CVE-2019-8649</a>

    <p>Sergei Glazunov upptäckte ett problem som kan leda till universell
    serveröverskridande skriptning.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8658">CVE-2019-8658</a>

    <p>akayn upptäckte ett problem som kan leda till universell serveröverskridande
    skriptning.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8666">CVE-2019-8666</a>

    <p>Zongming Wang och Zhe Jin upptäckte minneskorrumperingsproblem som
    kan leda till godtycklig kodexekvering.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8669">CVE-2019-8669</a>

    <p>akayn upptäckte minneskorrumperingsproblem som kan leda till
    godtycklig kodexekvering.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8671">CVE-2019-8671</a>

    <p>Apple upptäckte minneskorrumperingsproblem som kan leda till
    godtycklig kodexekvering.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8672">CVE-2019-8672</a>

    <p>Samuel Gross upptäckte minneskorrumperingsproblem som kan leda till
    godtycklig kodexekvering.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8673">CVE-2019-8673</a>

    <p>Soyeon Park och Wen Xu upptäckte minneskorrumperingsproblem som
    kan leda till godtycklig kodexekvering.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8676">CVE-2019-8676</a>

    <p>Soyeon Park och Wen Xu upptäckte minneskorrumperingsproblem som
    kan leda till godtycklig kodexekvering.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8677">CVE-2019-8677</a>

    <p>Jihui Lu upptäckte minneskorrumperingsproblem som kan leda till
    godtycklig kodexekvering.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8678">CVE-2019-8678</a>

    <p>An anonymous researcher, Anthony Lai, Ken Wong, Jeonghoon Shin,
    Johnny Yu, Chris Chan, Phil Mok, Alan Ho, och Byron Wai upptäckte
    minneskorrumperingsproblem som kan leda till exekvering av
    godtycklig kod.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8679">CVE-2019-8679</a>

    <p>Jihui Lu upptäckte minneskorrumperingsproblem som kan leda till
    godtycklig kodexekvering.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8680">CVE-2019-8680</a>

    <p>Jihui Lu upptäckte minneskorrumperingsproblem som kan leda till
    godtycklig kodexekvering.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8681">CVE-2019-8681</a>

    <p>G. Geshev upptäckte minneskorrumperingsproblem som kan leda till
    godtycklig kodexekvering.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8683">CVE-2019-8683</a>

    <p>lokihardt upptäckte minneskorrumperingsproblem som kan leda till
    godtycklig kodexekvering.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8684">CVE-2019-8684</a>

    <p>lokihardt upptäckte minneskorrumperingsproblem som kan leda till
    godtycklig kodexekvering.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8686">CVE-2019-8686</a>

    <p>G. Geshev upptäckte minneskorrumperingsproblem som kan leda till
    godtycklig kodexekvering.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8687">CVE-2019-8687</a>

    <p>Apple upptäckte minneskorrumperingsproblem som kan leda till
    godtycklig kodexekvering.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8688">CVE-2019-8688</a>

    <p>Insu Yun upptäckte minneskorrumperingsproblem som kan leda till
    godtycklig kodexekvering.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8689">CVE-2019-8689</a>

    <p>lokihardt upptäckte minneskorrumperingsproblem som kan leda till
    godtycklig kodexekvering.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8690">CVE-2019-8690</a>

    <p>Sergei Glazunov upptäckte ett problem som kan leda till universell
    serveröverskridande skriptning.</p></li>

</ul>

<p>Du kan se mer detaljer på WebKitGTK och WPE WebKit säkerhetsbulletin
WSA-2019-0004.</p>

<p>För den stabila utgåvan (Buster) har dessa problem rättats i
version 2.24.4-1~deb10u1.</p>

<p>Vi rekommenderar att ni uppgraderar era webkit2gtk-paket.</p>

<p>För detaljerad säkerhetsstatus om webkit2gtk vänligen se
dess säkerhetsspårare på
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4515.data"
