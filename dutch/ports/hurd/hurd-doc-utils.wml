#use wml::debian::template title="Debian GNU/Hurd --- Documentatie" NOHEADER="yes"
#include "$(ENGLISHDIR)/ports/hurd/menu.inc"
#use wml::debian::translation-check translation="cdc565969852e30580c3ad8ce3e7952261f03573"

<h1>Debian GNU/Hurd</h1>

<table border="2"
       summary="Index of GNU/Hurd utilities">

<caption><em>Index van GNU/Hurd-hulpprogramma's:</em></caption>

<tr>
  <th><a href="#syncfs" name="TOC_syncfs" type="text/html">
      Bestandssystemen synchroniseren</a></th>
  <th>&quot;<code>syncfs</code>&quot;</th>
</tr>
<tr>
  <th><a href="#showtrans" name="TOC_showtrans" type="text/html">
      De passieve translator van een BESTAND tonen</a></th>
  <th>&quot;<code>showtrans</code>&quot;</th>
</tr>
<tr>
  <th><a href="#devprobe" name="TOC_devprobe" type="text/html">
      Zoeken naar hardwareapparaten</a></th>
  <th>&quot;<code>devprobe</code>&quot;</th>
</tr>

#Uncomment and fill the blanks...
#<tr>
#  <th><a href="#" name="TOC_" type="text/html">
#      The  server</a></th>
#  <th>&quot;<code></code>&quot;</th>
#</tr>
</table>


<h2 class="center"><a href="#TOC_syncfs" name="syncfs" type="text/html">
Bestandssystemen synchroniseren - &quot;<code>syncfs</code>&quot;</a></h2>

<p>
<code>syncfs</code> kan worden gebruikt om de schrijfcache van schijfbestandssystemen leeg te maken.

<p>
Uitvoeren van &quot;<code>/bin/syncfs --help</code>&quot; geeft:
<br>
<pre>
Gebruik: syncfs [BESTAND...]
 Afdwingen dat alle hangende schrijfbewerkingen op schijf onmiddellijk worden uitgevoerd

  -s, --synchronous    Wachten tot alle schrijfbewerkingen op de schijf zijn voltooid
  -c, --no-children    Geen onderliggende bestandssystemen synchroniseren
  -?, --help           Deze hulplijst weergeven
      --usage          Een kort bericht over het gebruik weergeven
  -V, --version        Programmaversie weergeven

 Het bestandssysteem dat elk BESTAND bevat wordt gesynchroniseerd, evenals zijn
 onderliggende bestandssystemen, tenzij --no-children is gespecificeerd. Zonder
 argument BESTAND wordt het basisbestandssysteem gesynchroniseerd.

Meld bugs aan bug-hurd@gnu.org.
</pre>

<h2 class="center"><a href="#TOC_showtrans" name="showtrans" type="text/html">
De passieve translator van een BESTAND tonen - &quot;<code>showtrans</code>&quot;</a></h2>

<p>
Als u wilt weten welke passieve translator is verbonden met een eventuele inode,
gebruik dan <code>settrans</code> om dat uit te zoeken.

<p>
Uitvoeren van &quot;<code>/bin/showtrans --help</code>&quot; geeft:
<br>
<pre>
Gebruik: showtrans BESTAND...
 De passieve translator van BESTAND tonen...

  -p, --prefix        Altijd `BESTANDSNAAM: ' weergeven voor translators
  -P, --no-prefix     Nooit `BESTANDSNAAM: ' weergeven voor translators
  -s, --silent        Geen uitvoer; nuttig bij controleren van foutstatus
  -t, --translated    Alleen bestanden met translators weergeven

 Het argument BESTAND in de vorm van `-' geeft de translator weer op de node
 gekoppeld aan standaardinvoer.

</pre>

<h2 class="center"><a href="#TOC_devprobe" name="devprobe" type="text/html">
Zoeken naar hardwareapparaten - &quot;<code>devprobe</code>&quot;</a></h2>

<p>
Het hulpprogramma <code>devprobe</code> zoekt naar een of meerdere Mach-apparaten. Geef de namen op als argumenten op de commandoregel: als sommige apparaten bestaan, zal het hun namen één per regel echoën en succesvol afsluiten, anders zal het 1 teruggeven. De geboden opties maken het gemakkelijker om dit hulpprogramma in batchmodus te gebruiken.

<p>
Uitvoeren van &quot;<code>/bin/devprobe --help</code>&quot; geeft:
<br>
<pre>
Gebruik: devprobe APPARAATNAAM...
 Controleren op het bestaan van mach-apparaat APPARAATNAAM...

  -s, --silent                Gevonden apparaten niet weergeven
  -f, --first                 Stoppen na het eerste gevonden apparaat

 De afsluitcode is 0 als er apparaten werden gevonden.
</pre>

#Uncomment and fill the blanks...
#<h2 class="center"><a href="#TOC_" name="" type="text/html">
#The  server - &quot;<code></code>&quot;</a></h2>
#
#<p>
#A server for .
#
#<p>
#Running &quot;<code>/hurd/ --help</code>&quot; gives:
#<br>
#<pre>
#
#</pre>
