# SOME DESCRIPTIVE TITLE.
# Copyright ©
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2016-08-28 22:46+0700\n"
"Last-Translator: Izharul Haq <atoz.chevara@yahoo.com>\n"
"Language-Team: L10N Debian Indonesian <debian-l10n-indonesian@lists.debian."
"org>\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.6.10\n"

#: ../../english/template/debian/stats_tags.wml:6
msgid "Debian web site translation statistics"
msgstr "Statistik penerjemahan web site Debian"

#: ../../english/template/debian/stats_tags.wml:10
msgid "There are %d pages to translate."
msgstr "Ada %d halaman-halaman untuk diterjemahkan."

#: ../../english/template/debian/stats_tags.wml:14
msgid "There are %d bytes to translate."
msgstr "Ada %d bytes untuk diterjemahkan."

#: ../../english/template/debian/stats_tags.wml:18
msgid "There are %d strings to translate."
msgstr "Ada %d strings untuk diterjemahkan."

#: ../../stattrans.pl:282 ../../stattrans.pl:498
msgid "Wrong translation version"
msgstr "Versi terjemahan yang salah"

#: ../../stattrans.pl:284
msgid "This translation is too out of date"
msgstr "Terjemahan ini terlalu ketinggalan zaman"

#: ../../stattrans.pl:286
msgid "The original is newer than this translation"
msgstr "Aslinya lebih baru dari pada terjemahan ini"

#: ../../stattrans.pl:290 ../../stattrans.pl:498
msgid "The original no longer exists"
msgstr "Aslinya tidak ada lagi"

#: ../../stattrans.pl:474
msgid "hit count N/A"
msgstr "jumlah hit N/A"

#: ../../stattrans.pl:474
msgid "hits"
msgstr "hit"

#: ../../stattrans.pl:492 ../../stattrans.pl:493
msgid "Click to fetch diffstat data"
msgstr "Klik untuk mengambil data diffstat"

#: ../../stattrans.pl:603 ../../stattrans.pl:743
msgid "Created with <transstatslink>"
msgstr ""

#: ../../stattrans.pl:608
msgid "Translation summary for"
msgstr "Ringkasan terjemahan untuk"

#: ../../stattrans.pl:611 ../../stattrans.pl:767 ../../stattrans.pl:813
#: ../../stattrans.pl:856
msgid "Not translated"
msgstr "Tidak diterjemahkan"

#: ../../stattrans.pl:611 ../../stattrans.pl:766 ../../stattrans.pl:812
msgid "Outdated"
msgstr "Telah usang"

#: ../../stattrans.pl:611
msgid "Translated"
msgstr "Telah diterjemahkan"

#: ../../stattrans.pl:611 ../../stattrans.pl:691 ../../stattrans.pl:765
#: ../../stattrans.pl:811 ../../stattrans.pl:854
msgid "Up to date"
msgstr "Mutakhir"

#: ../../stattrans.pl:612 ../../stattrans.pl:613 ../../stattrans.pl:614
#: ../../stattrans.pl:615
msgid "files"
msgstr "berkas"

#: ../../stattrans.pl:618 ../../stattrans.pl:619 ../../stattrans.pl:620
#: ../../stattrans.pl:621
msgid "bytes"
msgstr "bytes"

#: ../../stattrans.pl:628
msgid ""
"Note: the lists of pages are sorted by popularity. Hover over the page name "
"to see the number of hits."
msgstr ""
"Catatan: daftar halaman diurutkan berdasarkan popularitas. Arahkan kursor ke "
"nama halaman untuk melihat jumlah hit."

#: ../../stattrans.pl:634
msgid "Outdated translations"
msgstr "Terjemahan telah usang"

#: ../../stattrans.pl:636 ../../stattrans.pl:690
msgid "File"
msgstr "Berkas"

#: ../../stattrans.pl:638
msgid "Diff"
msgstr "Diff"

#: ../../stattrans.pl:640
msgid "Comment"
msgstr "Komentar"

#: ../../stattrans.pl:641
msgid "Diffstat"
msgstr "Diffstat"

#: ../../stattrans.pl:642
msgid "Git command line"
msgstr ""

#: ../../stattrans.pl:644
msgid "Log"
msgstr "Log"

#: ../../stattrans.pl:645
msgid "Translation"
msgstr "Terjemahan"

#: ../../stattrans.pl:646
msgid "Maintainer"
msgstr "Pemelihara"

#: ../../stattrans.pl:648
msgid "Status"
msgstr "Status"

#: ../../stattrans.pl:649
msgid "Translator"
msgstr "Penerjemah"

#: ../../stattrans.pl:650
msgid "Date"
msgstr "Tanggal"

#: ../../stattrans.pl:657
msgid "General pages not translated"
msgstr "Halaman umum tidak diterjemahkan"

#: ../../stattrans.pl:658
msgid "Untranslated general pages"
msgstr "Halaman umum belum diterjemahkan"

#: ../../stattrans.pl:663
msgid "News items not translated"
msgstr "Item berita tidak diterjemahkan"

#: ../../stattrans.pl:664
msgid "Untranslated news items"
msgstr "Item berita belum diterjemahkan"

#: ../../stattrans.pl:669
msgid "Consultant/user pages not translated"
msgstr "Halaman konsultan/pengguna tidak diterjemahkan"

#: ../../stattrans.pl:670
msgid "Untranslated consultant/user pages"
msgstr "Halaman konsultan/pengguna belum diterjemahkan"

#: ../../stattrans.pl:675
msgid "International pages not translated"
msgstr "Halaman internasional tidak diterjemahkan"

#: ../../stattrans.pl:676
msgid "Untranslated international pages"
msgstr "Halaman internasional belum diterjemahkan"

#: ../../stattrans.pl:681
msgid "Translated pages (up-to-date)"
msgstr "Halaman telah diterjemahkan (up-to-date)"

#: ../../stattrans.pl:688 ../../stattrans.pl:838
msgid "Translated templates (PO files)"
msgstr "Tema telah diterjemahkan (berkas PO)"

#: ../../stattrans.pl:689 ../../stattrans.pl:841
msgid "PO Translation Statistics"
msgstr "Statistik Penerjemahan PO"

#: ../../stattrans.pl:692 ../../stattrans.pl:855
msgid "Fuzzy"
msgstr "Ragu-Ragu"

#: ../../stattrans.pl:693
msgid "Untranslated"
msgstr "Belum diterjemahkan"

#: ../../stattrans.pl:694
msgid "Total"
msgstr "Total"

#: ../../stattrans.pl:711
msgid "Total:"
msgstr "Total:"

#: ../../stattrans.pl:745
msgid "Translated web pages"
msgstr "Halaman web telah diterjemahkan"

#: ../../stattrans.pl:748
msgid "Translation Statistics by Page Count"
msgstr "Statistik Terjemahan berdasarkan Jumlah Halaman"

#: ../../stattrans.pl:763 ../../stattrans.pl:809 ../../stattrans.pl:853
msgid "Language"
msgstr "Bahasa"

#: ../../stattrans.pl:764 ../../stattrans.pl:810
msgid "Translations"
msgstr "Terjemahan"

#: ../../stattrans.pl:791
msgid "Translated web pages (by size)"
msgstr "Halaman web telah diterjemahkan (berdasarkan ukuran)"

#: ../../stattrans.pl:794
msgid "Translation Statistics by Page Size"
msgstr "Statistik Terjemahan berdasarkan Ukuran Halaman"

#~ msgid "Unified diff"
#~ msgstr "Unified diff"

#~ msgid "Colored diff"
#~ msgstr "Berwarna diff"

#, fuzzy
#~| msgid "Colored diff"
#~ msgid "Commit diff"
#~ msgstr "Berwarna diff"

#~ msgid "Created with"
#~ msgstr "Dibuat dengan"
