<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in the Apache HTTP server.
An attacker could send proxied requests to arbitrary servers, corrupt
memory in some setups involving third-party modules, and cause the
server to crash.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-34798">CVE-2021-34798</a>

    <p>Malformed requests may cause the server to dereference
    a NULL pointer.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-39275">CVE-2021-39275</a>

    <p>ap_escape_quotes() may write beyond the end of a buffer when given
    malicious input. No included modules pass untrusted data to these
    functions, but third-party / external modules may.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-40438">CVE-2021-40438</a>

    <p>A crafted request uri-path can cause mod_proxy to forward the
    request to an origin server choosen by the remote user.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
2.4.25-3+deb9u11.</p>

<p>We recommend that you upgrade your apache2 packages.</p>

<p>For the detailed security status of apache2 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/apache2">https://security-tracker.debian.org/tracker/apache2</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2776.data"
# $Id: $
