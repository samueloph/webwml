<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities have been discovered in the Tomcat
servlet and JSP engine.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30640">CVE-2021-30640</a>

   <p>A vulnerability in the JNDI Realm of Apache Tomcat allows an attacker to
   authenticate using variations of a valid user name and/or to bypass some of
   the protection provided by the LockOut Realm.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-33037">CVE-2021-33037</a>

   <p>Apache Tomcat did not correctly parse the HTTP transfer-encoding request
   header in some circumstances leading to the possibility to request
   smuggling when used with a reverse proxy. Specifically: - Tomcat
   incorrectly ignored the transfer encoding header if the client declared it
   would only accept an HTTP/1.0 response; - Tomcat honoured the identify
   encoding; and - Tomcat did not ensure that, if present, the chunked
   encoding was the final encoding.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
8.5.54-0+deb9u7.</p>

<p>We recommend that you upgrade your tomcat8 packages.</p>

<p>For the detailed security status of tomcat8 please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/tomcat8">https://security-tracker.debian.org/tracker/tomcat8</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2733.data"
# $Id: $
