<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two vulnerabilities were fixed in flac, the library for the 
Free Lossless Audio Codec.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6888">CVE-2017-6888</a>

    <p>Memory leak via a specially crafted FLAC file</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-0499">CVE-2020-0499</a>

    <p>Out of bounds read due to a heap buffer overflow</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
1.3.2-2+deb9u1.</p>

<p>We recommend that you upgrade your flac packages.</p>

<p>For the detailed security status of flac please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/flac">https://security-tracker.debian.org/tracker/flac</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2514.data"
# $Id: $
