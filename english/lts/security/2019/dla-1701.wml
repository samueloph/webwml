<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Juraj Somorovsky, Robert Merget and Nimrod Aviram discovered a padding
oracle attack in OpenSSL.</p>

<p>If an application encounters a fatal protocol error and then calls
SSL_shutdown() twice (once to send a close_notify, and once to receive
one) then OpenSSL can respond differently to the calling application
if a 0 byte record is received with invalid padding compared to if a 0
byte record is received with an invalid MAC. If the application then
behaves differently based on that in a way that is detectable to the
remote peer, then this amounts to a padding oracle that could be used
to decrypt data.</p>

<p>In order for this to be exploitable "non-stitched" ciphersuites must
be in use. Stitched ciphersuites are optimised implementations of
certain commonly used ciphersuites. Also the application must call
SSL_shutdown() twice even if a protocol error has occurred
(applications should not do this but some do anyway).
AEAD ciphersuites are not impacted.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.0.1t-1+deb8u11.</p>

<p>We recommend that you upgrade your openssl packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1701.data"
# $Id: $
