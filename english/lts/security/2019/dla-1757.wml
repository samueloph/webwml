<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there were a number of cross-site scripting
vulnerabilities (XSS) in cacti, a web-based front-end for the RRDTool
monitoring tool.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11025">CVE-2019-11025</a>

    <p>In clearFilter() in utilities.php in Cacti before 1.2.3, no escaping
	occurs before printing out the value of the SNMP community string
	(SNMP Options) in the View poller cache, leading to XSS.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
0.8.8b+dfsg-8+deb8u7.</p>

<p>We recommend that you upgrade your cacti packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1757.data"
# $Id: $
