<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>A remote denial of service vulnerability has been discovered in
graphicsmagick, a collection of image processing tools and associated
libraries.</p>

<p>A specially crafted file can be used to produce a heap-based buffer
overflow and application crash by exploiting a defect in the
AcquireCacheNexus function in magick/pixel_cache.c.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.3.16-1.1+deb7u14.</p>

<p>We recommend that you upgrade your graphicsmagick packages.
<p><b>Note</b>: The previous graphicsmagick package inadvertently introduced a
dependency on liblcms2-2. This version of the package returns to using
liblcms1.  If your system does not otherwise require liblcms2-2, you
may want to consider removing it following the graphicsmagick upgrade.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1168.data"
# $Id: $
