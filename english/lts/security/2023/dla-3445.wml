<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two vulnerabilities were fixed in GNU cpio, a program to manage
archives of files.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14866">CVE-2019-14866</a>

    <p>Improper validation of input files when generatingtar archives.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-38185">CVE-2021-38185</a>

    <p>Arbitrary code via crafted pattern file.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
2.12+dfsg-9+deb10u1.</p>

<p>We recommend that you upgrade your cpio packages.</p>

<p>For the detailed security status of cpio please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/cpio">https://security-tracker.debian.org/tracker/cpio</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3445.data"
# $Id: $
