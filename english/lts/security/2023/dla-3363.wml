<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple out-of-bounds read vulnerabilities were found in pcre2, a Perl
Compatible Regular Expression library, which could result in information
disclosure or denial or service.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20454">CVE-2019-20454</a>

    <p>Out-of-bounds read when the pattern <code>&bsol;X</code> is JIT
    compiled and used to match specially crafted subjects in non-UTF
    mode.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1586">CVE-2022-1586</a>

    <p>Out-of-bounds read involving unicode property matching in
    JIT-compiled regular expressions. The issue occurs because the
    character was not fully read in case-less matching within JIT.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1587">CVE-2022-1587</a>

    <p>Out-of-bounds read affecting recursions in JIT-compiled regular
    expressions caused by duplicate data transfers.</p>

<p>This upload also fixes a subject buffer overread in JIT when UTF is
disabled and <code>&bsol;X</code> or <code>&bsol;R</code> has a greater
than 1 fixed quantifier. This issue was found by Yunho Kim.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
10.32-5+deb10u1.</p>

<p>We recommend that you upgrade your pcre2 packages.</p>

<p>For the detailed security status of pcre2 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/pcre2">https://security-tracker.debian.org/tracker/pcre2</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3363.data"
# $Id: $
