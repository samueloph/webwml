<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been fixed in the libstb library.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16981">CVE-2018-16981</a>

    <p>Heap-based buffer overflow in stbi__out_gif_code().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13217">CVE-2019-13217</a>

    <p>Heap buffer overflow in the Vorbis start_decoder().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13218">CVE-2019-13218</a>

    <p>Division by zero in the Vorbis predict_point().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13219">CVE-2019-13219</a>

    <p>NULL pointer dereference in the Vorbis get_window().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13220">CVE-2019-13220</a>

    <p>Uninitialized stack variables in the Vorbis start_decoder().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13221">CVE-2019-13221</a>

    <p>Buffer overflow in the Vorbis compute_codewords().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13222">CVE-2019-13222</a>

    <p>Out-of-bounds read of a global buffer in the Vorbis draw_line().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13223">CVE-2019-13223</a>

    <p>Reachable assertion in the Vorbis lookup1_values().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-28021">CVE-2021-28021</a>

    <p>Buffer overflow in stbi__extend_receive().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-37789">CVE-2021-37789</a>

    <p>Heap-based buffer overflow in stbi__jpeg_load().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-42715">CVE-2021-42715</a>

    <p>The HDR loader parsed truncated end-of-file RLE scanlines as an
    infinite sequence of zero-length runs.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-28041">CVE-2022-28041</a>

    <p>Integer overflow in stbi__jpeg_decode_block_prog_dc().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-28042">CVE-2022-28042</a>

    <p>Heap-based use-after-free in stbi__jpeg_huff_decode().</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
0.0~git20180212.15.e6afb9c-1+deb10u1.</p>

<p>We recommend that you upgrade your libstb packages.</p>

<p>For the detailed security status of libstb please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libstb">https://security-tracker.debian.org/tracker/libstb</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3305.data"
# $Id: $
