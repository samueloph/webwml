<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security issues have been found in Icedove, Debian's version of
the Mozilla Thunderbird mail client: Multiple memory safety errors may
lead to the execution of arbitrary code or denial of service.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1:45.2.0-2~deb7u1.</p>

<p>We recommend that you upgrade your icedove packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-572.data"
# $Id: $
