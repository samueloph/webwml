<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there were three vulnerabilities in Pillow, an
imaging library for the Python programming language</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19911">CVE-2019-19911</a>

    <p>There is a DoS vulnerability in Pillow before 6.2.2 caused by
    FpxImagePlugin.py calling the range function on an unvalidated 32-bit
    integer if the number of bands is large. On Windows running 32-bit Python,
    this results in an OverflowError or MemoryError due to the 2 GB limit.
    However, on Linux running 64-bit Python this results in the process being
    terminated by the OOM killer.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-5312">CVE-2020-5312</a>

    <p>libImaging/PcxDecode.c in Pillow before 6.2.2 has a PCX P mode buffer
    overflow.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-5312">CVE-2020-5313</a>

    <p>libImaging/FliDecode.c in Pillow before 6.2.2 has an FLI buffer
    overflow.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
2.6.1-2+deb8u4.</p>

<p>We recommend that you upgrade your pillow packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2057.data"
# $Id: $
