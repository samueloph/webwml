<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A vulnerability was discovered in roundcube, a skinnable AJAX based
webmail solution for IMAP servers.  HTML messages with malicious svg or
math content can exploit a Cross-site scripting (XSS) vulnerability.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1.2.3+dfsg.1-4+deb9u7.</p>

<p>We recommend that you upgrade your roundcube packages.</p>

<p>For the detailed security status of roundcube please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/roundcube">https://security-tracker.debian.org/tracker/roundcube</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2322.data"
# $Id: $
