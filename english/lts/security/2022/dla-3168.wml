<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An issue has been found in openvswitch, a software-based,  Ethernet
virtual switch.</p>

<p>This issue is about a heap buffer over-read in flow.c, which could lead
to access to an unmapped region of memory. This could result in crashing
the software, memory modification, or possible remote execution.</p>


<p>For Debian 10 buster, this problem has been fixed in version
2.10.7+ds1-0+deb10u2.</p>

<p>We recommend that you upgrade your openvswitch packages.</p>

<p>For the detailed security status of openvswitch please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/openvswitch">https://security-tracker.debian.org/tracker/openvswitch</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3168.data"
# $Id: $
