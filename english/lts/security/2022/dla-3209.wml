<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that ini4j, a Java library for handling the Windows ini file
format, was vulnerable to a denial of service attack via the fetch() method in
BasicProfile class, if an attacker provided a manipulated ini file.</p>

<p>For Debian 10 buster, this problem has been fixed in version
0.5.4-1~deb10u1.</p>

<p>We recommend that you upgrade your ini4j packages.</p>

<p>For the detailed security status of ini4j please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/ini4j">https://security-tracker.debian.org/tracker/ini4j</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3209.data"
# $Id: $
