<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was an out-of-bounds read and integer
underflow vulnerability in open vSwitch, a software-based Ethernet
virtual switch</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-4337">CVE-2022-4337</a>

    <p>Out-of-Bounds Read in Organization Specific TLV</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-4338">CVE-2022-4338</a>

    <p>Integer Underflow in Organization Specific TLV</p></li>

</ul>

<p>For Debian 10 <q>Buster</q>, these problems have been fixed in version
2.10.7+ds1-0+deb10u3.</p>

<p>We recommend that you upgrade your openvswitch packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3253.data"
# $Id: $
