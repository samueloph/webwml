<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a potential remote denial-of-service
vulnerability in the knot-resolver DNSSEC-validating DNS resolver.</p>

<p>Remote attackers could have caused a denial of service via CPU consumption
by exploiting algorithmic complexity: during an attack, an authoritative server
would return large nameserver or address sets.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-40188">CVE-2022-40188</a>

    <p>Knot Resolver before 5.5.3 allows remote attackers to cause a denial of
    service (CPU consumption) because of algorithmic complexity. During an
    attack, an authoritative server must return large NS sets or address
    sets.</p></li>

</ul>

<p>For Debian 10 <q>Buster</q>, these problems have been fixed in version
3.2.1-3+deb10u1.</p>

<p>We recommend that you upgrade your knot-resolver packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3139.data"
# $Id: $
