#use wml::debian::template title="Interpreting the Debian Code of Conduct" BARETITLE=true

{#meta#:
<meta name="keywords" content="code of conduct, coc">
:#meta#}

<h2>Some General Comments</h2>

<p>The purpose of this document is to provide some explanation and
examples of how the <a href="$(HOME)/code_of_conduct">Code of
Conduct</a> (CoC) is interpreted within the Debian Project. If you
have any questions, please reach out to the Community Team
(community@debian.org). If you are worried something you are thinking
about doing might violate the CoC, please also reach out to the
Community Team.</p>

<p>The purpose of the CoC is to create a community space where people
feel comfortable. This helps us maintain a collective of contributors
who are excited to participate in Debian and help us fulfil our goals
of creating and maintaining Debian. The Debian community is, at the
same time, both a group of friends working on a project and a group of
colleagues doing their jobs. Debian is as much a huge social grouping
as a technical project.</p>

<p>The goal of the CoC, and CoC enforcement efforts, is to help people
align to the shared values Debian has adopted as a community through
the General Resolution process. You have to follow and respect the CoC
in order to participate in Debian. You do not have to be perfect --
everyone makes mistakes or has a bad day -- the goal of CoC
enforcement is to help people to do better. That is all that's being
asked of you: try your best to treat your friends and colleagues with
consideration. The CoC covers all activities within Debian and those
you carry out as a representative of the Debian Project. Punitive
actions, such as (temporary or permanent) banning or loss of status,
may also occur if your non-Debian activities impact the Project or
create an unsafe or harmful space in Debian.</p>

<p>This is a <strong>living document</strong>. It will change over
time as what is considered normal and ideal both inside and outside
Debian evolves. The Debian Code of Conduct was initially ratified in
2014. It has not changed since then, though expectations and best
practices in the free and open source software communities, and in
tech in general, have.</p>

<h2>1. Be respectful</h2>

<p><strong>In a project the size of Debian, inevitably there will be
people with whom you may disagree, or find it difficult to
cooperate. Accept that, but even so, remain respectful. Disagreement
with someone's actions or opinions is no excuse for poor behaviour or
personal attacks. A community in which people feel threatened is not a
healthy community.</strong></p>

<p>Every member of the community and the wider world deserves
respect. Respect is not something to be earned in Debian, it is
something each and every member of the Community deserves, regardless
of their age, gender, body size, education, ethnicity, or other
factors.</p>

<p>Debian is an open community. All cultures and beliefs are welcome
and acknowledged so long as they are not harming others. Your own
expectations or your own cultural background are not an excuse to
violate the CoC or be disrespectful to another person within the
Debian community or within your role as a member of the Debian
community. Debian has its own culture. When working with Debian
contributors or users, please abide by Debian norms and represent
Debian positively.</p>

<p>People in Debian come from different cultural backgrounds, have
different experiences, and might not be fluent or comfortable in the
language of a given discussion. This means that it is important to
assume best intentions (see below) and be understanding about
differences in communication styles. This does not, however, mean it
is acceptable to intentionally communicate inappropriately or not
change your communication style to meet community norms once it has
been brought up.</p>

<h3>Examples of Disrespectful Behaviour</h3>

<p>The following is a <strong>non-exhaustive</strong> list of examples
of disrespectful behaviour:</p>

<ul>
  <li>Profanity directed at a person or their work</li>
  <li>Insulting someone for their age, disability, gender, sexuality,
      body size, religion, nationality, race, ethnicity, caste, tribe,
      education, contribution types, or status within Debian and/or
      free and open source software, using one of the above as an
      insult, or making derogatory statements about a group</li>
  <li>Intentionally using the wrong pronouns or name
      (e.g. <q>deadnaming</q>) of an individual</li>
  <li>Aggressively or repeatedly contacting someone after being asked
      to stop</li>
  <li>Not making good faith efforts to reach agreement with people or
      to change behaviours that are counter to Debian's values</li>
</ul>

<h2>2. Assume good faith</h2>

<p><strong>Debian Contributors have many ways of reaching our common goal of a
free operating system: someone else's ways of doing something may
differ from your ways. Assume that other people are working
collaboratively towards this goal. Note that many of our Contributors
are not native English speakers or may have different cultural
backgrounds.</strong></p>

<p><strong>Debian is a global project.</strong> Debian includes people
from many different backgrounds, experiences, styles of communication
and cultural norms. As such, it is particularly important to assume
good faith. This means to assume, as is reasonable, that the person
you're talking with is not trying to hurt you or insult you.</p>

<p>In order to assume good faith, we must also act in good faith. This
also means that you should assume someone is trying their best, and
that you should not hurt or insult them. Intentionally upsetting
someone within Debian is not acceptable.</p>

<p>Assuming good faith includes communication, behaviour, and
contribution. This means assuming that everyone contributing, whatever
their contributions, is putting in the effort they are capable of and
doing so with integrity.</p>

<h3>Examples of Non-Good Faith Behaviours</h3>

<p>Again, the following is a <strong>non-exhaustive</strong> list:</p>

<ul>
  <li>Trolling</li>
  <li>Assuming someone is trolling</li>
  <li>Using phrases like <q>I know you're not stupid</q> or <q>You
      couldn't have done this intentionally, so you must be
      stupid</q></li>
  <li>Insulting someone's contributions</li>
  <li>Stirring up antagonism in others - <q>punching people's
      buttons</q> to produce an effect</li>
</ul>

<h2>3. Be collaborative</h2>

<p><strong>Debian is a large and complex project; there is always more
to learn within Debian. It's good to ask for help when you need
it. Similarly, offers for help should be seen in the context of our
shared goal of improving Debian.</strong></p>

<p><strong>When you make something for the benefit of the project, be
willing to explain to others how it works, so that they can build on
your work to make it even better.</strong></p>

<p>Our contributions help other contributors, the project and our
users. We work in the open under the ethos that anyone who wants to
contribute should be able to, within reason. Everyone in Debian has a
different background and different skills. This means you should be
positive and constructive and, whenever possible, you should provide
assistance, advice, or mentorship. We value consensus, though there
are times when democratic decisions will be made or direction may be
decided by those people who are willing and able to undertake an
activity.</p>

<p>Different teams use different tools and have different norms in
collaboration. This could mean things like weekly synchronous
meetings, shared notes, or code review processes. Just because things
have been done a certain way doesn't mean it's the best or only way to
do things, and teams should be open to discussing new collaboration
methods.</p>

<p>Part of being collaborative is also a good faith assumption (see
above) that others are being collaborative as well rather than
assuming that others are <q>out to get you</q>, or ignoring you.</p>

<p>Good collaboration is more valuable than technical skills. Being a
good technical contributor does not make it acceptable to be a harmful
community member.</p>

<h3>Examples of Bad Collaboration</h3>

<ul>
  <li>Refusing to adopt a team's contribution norms</li>
  <li>Insulting other contributors / colleagues</li>
  <li>Refusing to work with others unless it poses a threat to your
    safety or well-being</li>
</ul>

<h2>4. Try to be concise</h2>

<p><strong>Keep in mind that what you write once will be read by
hundreds of people. Writing a short email means people can understand
the conversation as efficiently as possible. When a long explanation
is necessary, consider adding a summary.</strong></p>

<p><strong>Try to bring new arguments to a conversation so that each
mail adds something unique to the thread, keeping in mind that the
rest of the thread still contains the other messages with the
arguments that have already been made.</strong></p>

<p><strong>Try to stay on topic, especially in discussions that are
already fairly large.</strong></p>

<p>Certain topics are not appropriate for Debian, including some
contentious topics of a political or religious nature. Debian is an
environment of colleagues as much as it is one of friends. Public
collective exchanges should be respectful, on-topic, and
professional. Using concise, accessible language is important,
especially as many Debian contributors are non-native English
speakers, and much of the project's communications are in English. It
is important to be clear and explicit, and when possible explain or
avoid idioms. (Note: Using idioms, for example, is not a violation of
the Code of Conduct. Avoiding them is just a good practice in general
for clarity.)</p>

<p>It is not always easy to convey meaning and tone over text or
across cultures. Being open-minded, assuming good intentions, and
trying are the most important things in conversation.</p>

<h2>5. Be open</h2>

<p><strong>Most ways of communication used within Debian allow for
public and private communication. As per paragraph three of the social
contract, you should preferably use public methods of communication
for Debian-related messages, unless posting something
sensitive.</strong></p>

<p><strong>This applies to messages for help or Debian-related
support, too; not only is a public support request much more likely to
result in an answer to your question, it also makes sure that any
inadvertent mistakes made by people answering your question will be
more easily detected and corrected.</strong></p>

<p>It is important to keep as many communications public as
possible. Many Debian mailing lists can be joined by anyone or have
publicly accessible archives. Archives may be recorded and stored by
non-Debian sources (e.g. the Internet Archive). It should be assumed
that what has been said on a Debian mailing list
is <q>permanent</q>. Many people keep logs of IRC conversations as
well.</p>

<h3>Privacy</h3>

<p>Private conversations within the context of the project are still
considered to be under the Code of Conduct. Reporting that something
said in a private conversation is inappropriate or unsafe (see above
for examples) is encouraged.</p>

<p>At the same time, it is important to respect private conversations,
and they should not be shared barring issues of safety. Certain
places, like the debian-private mailing list, fall under this
category.</p>

<h2>6. In case of problems</h2>

<p><strong>While this code of conduct should be adhered to by
participants, we recognize that sometimes people may have a bad day,
or be unaware of some of the guidelines in this code of conduct. When
that happens, you may reply to them and point out this code of
conduct. Such messages may be in public or in private, whatever is
most appropriate. However, regardless of whether the message is public
or not, it should still adhere to the relevant parts of this code of
conduct; in particular, it should not be abusive or
disrespectful. Assume good faith; it is more likely that participants
are unaware of their bad behaviour than that they intentionally try to
degrade the quality of the discussion.</strong></p>

<p><strong>Serious or persistent offenders will be temporarily or
permanently banned from communicating through Debian's
systems. Complaints should be made (in private) to the administrators
of the Debian communication forum in question. To find contact
information for these administrators, please see the page on Debian's
organisational structure.</strong></p>

<p>The purpose of the Code of Conduct is to provide guidance for
people on how to maintain Debian as a welcoming community. People
should feel welcome to participate as they are and not as others
expect them to be. The Debian Code of Conduct outlines things people
should do, rather than ways they should not behave. This document
provides insight on how the Community Team interprets the Code of
Conduct. Different members of the Debian community may interpret this
document differently. What is most important is that people feel
comfortable, safe, and welcome within Debian. Regardless of whether it
is called out specifically in this document, if someone does not feel
comfortable, safe, and/or welcome, they should reach out to the <a
href="https://wiki.debian.org/Teams/Community">Community Team</a>.</p>

<p>If someone is concerned that they may have done something
inappropriate or are thinking about doing something they think may be
inappropriate, they are also encouraged to reach out to the Community
Team.</p>

<p>As Debian is so large, the Community Team can not and does not
proactively monitor all communications, though sometimes members may
see them in passing. As such, it is important for the Debian Community
to work with the Community Team.</p>

<h2>Failing to Follow the Code of Conduct</h2>

<p>No one is expected to be perfect all the time. Making a mistake is
not the end of the world, though it may result in someone reaching out
to ask for improvement. Within Debian there is an expectation of good
faith efforts to do well and do better. Repeated violation of the CoC
may result in reprisal or restrictions on community interaction
including, but not limited to:</p>

<ul>
  <li>official warnings;</li>
  <li>temporary or permanent bans on email lists, IRC channels, or
  other communication medial</li>
  <li>temporary or permanent removal of rights and privileges; or</li>
  <li>temporary or permanent demotion of status within the Debian
  Project.</li>
</ul>
